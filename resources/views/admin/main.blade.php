<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @isset($module)
        <meta name="module" content="{{$module}}">
    @endisset

    <title>Perumar</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>
    <main class="container-fluid p-0 d-flex">
        <section class="left_colum">
            <section class="brand">
                <p id="brand--logo">PERUMAR</p>
            </section>
            <section class="menu">
                <ul class="menu_list">
                    <li class="menu_list--item">
                        <i id="i_import" title="Importaciones" class="fa fa-custom fa-th-large" aria-hidden="true"></i>
                        <a href="{{ route('imports.index') }}">Importaciones</a>
                        {{-- <ul class="submenu_list">
                            <li class="submenu_list--item"><a href="{{ route('imports.index') }}"> Registros </a></li> --}}
                            <!--<li class="submenu_list--item"><a href="{{ route('shipper.index') }}"> Shipper </a></li>
                            <li class="submenu_list--item"><a href="{{ route('consignee.index') }}"> Consignee </a></li>-->
                        {{-- </ul> --}}
                    </li>
                    <li class="menu_list--item">
                        <i id="i_export" title="Exportaciones" class="fa fa-custom fa-th-large" aria-hidden="true"></i>
                        <a href="{{ route('exports.index') }}">Exportaciones</a>
                        {{-- <ul class="submenu_list">
                            <li class="submenu_list--item"><a href="{{ route('exports.index') }}"> Registros </a></li>
                        </ul> --}}
                    </li>
                    <li class="menu_list--item">
                        <i id="i_log" title="Logistico" class="fa fa-custom fa-th-large" aria-hidden="true"></i>
                        <a href="{{ route('logistics.index') }}">Logistico</a>
                    </li>
                    <!--<li class="menu_list--item">
                        <i id="i_clients" title="Clientes" class="fa fa-custom fa-users" aria-hidden="true"></i>
                        <a href="{{ route('clients.index') }}">Clientes</a>
                    </li>-->
                    @if (Auth::user()->permissions != 'Cliente')
                        <li class="menu_list--item">
                            <i id="i_users" title="Usuarios" class="fa fa-custom fa-users" aria-hidden="true"></i>
                            <a href="{{ route('users.index') }}">Usuarios</a>
                        </li>
                    @endif

                </ul>
            </section>
            <section class="left_colum--footer">
                <ul class="options_list d-flex">
                    <li class="options_list--item"><i title="Ayuda" class="fa fa-custom fa-question-circle" aria-hidden="true"></i></li>
                    <li class="options_list--item"><i title="Salir" onclick="document.getElementById('logout-form').submit();" class="fa fa-custom fa-sign-out" aria-hidden="true"></i></li>
                    <li class="options_list--item"><i id="minimize_sidebar" title="Minimizar" class="fa fa-custom fa-arrow-left" aria-hidden="true"></i></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </section>
        </section>
        <section class="right_column">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @yield('content')
        </section>
    </main>
    </div>
    <script src="{{ asset('js/app.js') }}" ></script>
    @yield('scripts')
</body>
</html>
