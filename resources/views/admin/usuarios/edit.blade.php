@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-header"><h2>Editar Usuario/Cliente</h2></div>
                    <div class="card-body">
                        {!! Form::model($user, ['method'=>'PUT',  'files'=> true, 'route' => ['users.update', $user]]) !!}
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    {!! Form::label('Nombre') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group col-md-12">
                                    {!! Form::label('Email') !!}
                                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group col-md-12">
                                    {!! Form::label('Password') !!}
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group  col-md-12">
                                    {!! Form::label('Nivel de permiso') !!}
                                    {!! Form::select('permissions', ['Superadmin'=>'Superadmin','Admin'=>'Admin', 'Cliente' => 'Cliente'], null, ['placeholder' => 'Selecciona el nivel de permiso', 'class' => 'form-control']) !!}
                                </div>
                                {!!form::submit('Editar',['class' => 'btn btn-md btn-dark'])!!}

                                {!!form::button('Cancelar',['id'=>'cancel','content'=>'Cancelar','class'=>'btn btn-default btn-md'])!!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        document.querySelector('#cancel').addEventListener('click', e =>{
            document.location.href = "{{ route('users.index')}}";
        })
    </script>
@stop
