@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert">
                    <strong>{{ session('message') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header"><h2>Usuarios/Clientes</h2></div>
                <div class="card-body">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <th>NOMBRE</th>
                            <th>EMAIL</th>
                            <th>NIVEL PERMISO</th>
                            <th>ACCION</th>
                        </thead>
                        <tbody>
                             @if($users->count() > 0)
                                @foreach ($users as $user)
                                    <tr
                                        @if (auth()->user()->email === $user->email && auth()->user()->name === $user->name)
                                            style="background-color:lightblue;"
                                        @endif
                                    >
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->permissions}}</td>
                                        <td>
                                            @if (auth()->user()->permissions == 'Admin')
                                                @if ($user->permissions !== 'Superadmin')
                                                    <a href="{{route('users.edit', $user->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                @endif
                                            @else
                                                <a href="{{route('users.edit', $user->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteUser({{$user->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                                {!! Form::open(['route' => ['users.destroy', $user->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_user_'.$user->id]) !!}
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan = 25 align="center">No tiene usuarios</td></tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="d-flex absolute-center">
                        {{$users->links()}}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" id="adduser"  name="adduser" class="btn btn-dark " >Agregar Usuarios</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function actions(){
            let addBtn = document.querySelector('#adduser');

            addBtn.addEventListener('click', e =>{
                document.location.href = "{{ route('users.create')}}";
            });
            this.deleteUser = (id) => {
                confirm('¿Seguro que desea eliminar este registro?') ? document.forms['delete_user_'+ id].submit() : null;
            }
        }
        actions();
    </script>
@stop
