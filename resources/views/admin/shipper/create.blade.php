@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-sm-12 pb-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Agregar shippper</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['id' => 'form_create', 'route' => 'shippers.store', 'method'=>'POST']) !!}
                            <div class="form-group">
                                {!! Form::label('Nombre') !!}
                                {!! Form::text('nombre', null, ['class' => 'form-control', ]) !!}
                            </div>
                            {!! Form::text('type', 'import', ['class' => 'd-none']) !!}
                            {!!form::submit('Crear',['id'=>'btn','class'=>'btn btn-dark btn-md'])!!}

                            {!!form::button('Cancelar',['id'=>'cancel','content'=>'Cancelar','class'=>'btn btn-default btn-md'])!!}
                         {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')

    <script>
        document.querySelector('#cancel').addEventListener('click', e =>{
            document.location.href = "{{ route('shippers.index')}}";
        })
    </script>
@stop
