@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header"><h2>Shippers</h2></div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <thead>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>ACCION</th>
                        </thead>
                        <tbody>
                            @if($shippers->count() > 0)
                                @foreach ($shippers as $shipper)
                                    <tr>
                                        <td>{{$shipper->id}}</td>
                                        <td>{{$shipper->nombre}}</td>
                                        <td class="d-flex">
                                            <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteShipper({{$shipper->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                            {!! Form::open(['route' => ['shippers.destroy', $shipper->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_shipper_'.$shipper->id]) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan = 25 align="center">No tiene shippers</td></tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="d-flex absolute-center">
                        {{$shippers->links()}}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" id="addshipper"  name="addshipper" class="btn btn-dark btn-" >Agregar shipper</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function actions(){
            let addBtn = document.querySelector('#addshipper');

            addBtn.addEventListener('click', e =>{
                document.location.href = "{{ route('shippers.create')}}";
            });
            this.deleteShipper = id => {

                confirm('¿Seguro que desea eliminar este registro?')  ? document.forms['delete_shipper_'+ id].submit() : null;
            }
        }
        actions();
    </script>
@stop