@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header"><h2>Consignees</h2></div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <tbody>
                            @if($consignees->count() > 0)
                                @foreach ($consignees as $consignee)
                                    <tr>
                                        <td>{{$consignee->id}}</td>
                                        <td>{{$consignee->nombre}}</td>
                                        <td class="d-flex">
                                            <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteConsignee({{$consignee->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                            {!! Form::open(['route' => ['consignees.destroy', $consignee->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_consignee_'.$consignee->id]) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan = 25 align="center">No tiene consignees</td></tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="d-flex absolute-center">
                        {{$consignees->links()}}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" id="addconsignee"  name="addconsignee" class="btn btn-dark btn-" >Agregar consignee</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function actions(){
            let addBtn = document.querySelector('#addconsignee');

            addBtn.addEventListener('click', e =>{
                document.location.href = "{{ route('consignees.create')}}";
            });
            this.deleteConsignee = id => {

                confirm('¿Seguro que desea eliminar este registro?')  ? document.forms['delete_consignee_'+ id].submit() : null;
            }
        }
        actions();
    </script>
@stop