@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header"><h2>Clientes</h2></div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover table-sm table-responsive">
                        <thead>
                            <th>NRO EXPORTACION</th>
                            <th>BOOKING</th>
                            <th>VESSEL</th>
                            <th>ETA CALLAO</th>
                            <th>CONSIGNATARIO</th>
                            <th>PUERTO DESTINO</th>
                            <th>CANTIDAD CTNS</th>
                            <th>F. ZARPE</th>
                            <th>PUERTO</th>
                            <th>ETA</th>
                            <th>ETD</th>
                            <th>NAVE TRANSBORDO</th>
                            <th>DESTINO FINAL</th>
                            <th>ETA DESTINO</th>
                            <th>PERUMAR N° EXPORTACION</th>
                            <th>CONTENEDORES</th>
                            <th>OBSERVACIONES</th>
                            <th>ACCIÓN</th>
                        </thead>
                        <tbody>
                             @if($clients->count() > 0)
                                @foreach ($clients as $client)
                                    <tr>
                                        <td>{{$client->nro_exportacion}}</td>
                                        <td>{{$client->booking}}</td>
                                        <td>{{$client->vessel}}</td>
                                        <td>{{$client->eta_callao}}</td>
                                        <td>{{$client->consignatario}}</td>
                                        <td>{{$client->pod}}</td>
                                        <td>{{$client->cantidad}}</td>
                                        <td>{{$client->fec_zarpe}}</td>
                                        <td>{{$client->puerto}}</td>
                                        <td>{{$client->eta}}</td>
                                        <td>{{$client->etd}}</td>
                                        <td>{{$client->nave}}</td>
                                        <td>{{$client->destino}}</td>
                                        <td>{{$client->eta_destino}}</td>
                                        <td>{{$client->perumar_n_export}}</td>
                                        <td>{{str_replace('-', '', $client->contenedores)}}</td>
                                        <td>{{str_replace('-', '', $client->observaciones)}}</td>
                                        <td class="d-flex">
                                            <!--<a data-id="{{$client->id}}" class="show-containers btn btn-dark btn-sm" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Mostrar contenedores"><i class="fa fa-th-large" aria-hidden="true"></i></a>-->
                                            <a href="{{route('clients.edit', $client->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteClient({{$client->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                            {!! Form::open(['route' => ['clients.destroy', $client->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_client_'.$client->id]) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan = 25 align="center">No tiene clientes</td></tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="d-flex absolute-center">
                        {{$clients->links()}}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" id="addclient"  name="addclient" class="btn btn-dark" >Agregar Cliente</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function actions(){
            let addBtn = document.querySelector('#addclient');

            addBtn.addEventListener('click', e =>{
                document.location.href = "{{ route('clients.create')}}";
            });
            this.deleteClient= (id) => {
                confirm('¿Seguro que desea eliminar este registro?') ? document.forms['delete_client_'+ id].submit() : null;
            }
        }
        actions();
    </script>
@stop
