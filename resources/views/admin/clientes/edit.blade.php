@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-sm-12 pb-5">
                <div id="alert" class="alert alert-success alert-dismissible fade d-none" role="alert">
                    <strong>Recurso creado con exito!</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">
                                    <h4>Agregar Clientes</h4>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="email-tab" data-toggle="tab" href="#email" role="tab" aria-controls="email" aria-selected="false">
                                    <h4>Enviar Email</h4>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                         <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="create" role="tabpanel" aria-labelledby="create-tab">
                                {!! Form::model($client ,['route' => ['clients.update', $client], 'method'=>'PUT','class'=> '_form',  'files'=> true, 'id' => 'form_update']) !!}
                                    <div class="form-row">
                                        {!! Form::text('id', null, ['class' => 'd-none', 'id' => 'id']) !!}
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Nro Exportacion') !!}
                                            {!! Form::text('nro_exportacion', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Booking') !!}
                                            {!! Form::text('booking', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Vessel') !!}
                                            {!! Form::text('vessel', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('ETA callao') !!}
                                            {!! Form::text('eta_callao', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Consignatario') !!}
                                            {!! Form::text('consignatario', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Puerto destino') !!}
                                            {!! Form::text('pod', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Cantidad ctns') !!}
                                            {!! Form::number('cantidad', null, ['class' => 'form-control', 'min' => 0, 'onkeydown'=> 'javascript: return event.keyCode == 69 ? false : true']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('F. Zarpe') !!}
                                            {!! Form::date('fec_zarpe', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Puerto') !!}
                                            {!! Form::text('puerto', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('ETA') !!}
                                            {!! Form::text('eta', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('ETD') !!}
                                            {!! Form::text('etd', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Nave transbordo') !!}
                                            {!! Form::text('nave', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Destino final') !!}
                                            {!! Form::text('destino', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4">
                                            {!! Form::label('ETA destino') !!}
                                            {!! Form::text('eta_destino', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4">
                                            {!! Form::label('Perumar N° exportacion') !!}
                                            {!! Form::text('perumar_n_export', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6">
                                            {!! Form::label('Contenedores') !!}
                                            {!! Form::textarea('contenedores', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6">
                                            {!! Form::label('Observaciones') !!}
                                            {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
                                        </div>
                                        {!!form::button('Editar',['id'=>'btn_update','class'=>'btn btn-dark btn-md d-flex'])!!}

                                        {!!form::button('Cancelar',['id'=>'cancel','content'=>'Cancelar','class'=>'btn btn-default btn-md'])!!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade" id="email" role="tabpanel" aria-labelledby="email-tab">
                                {!! Form::open(['id' => 'form_send_email']) !!}
                                    <div class="form-row">
                                        <div class="form-group col-sm-12">
                                            {!! Form::label('Email') !!}
                                            {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-12">
                                            {!! Form::label('Mensaje') !!}
                                            {!! Form::textarea('mensaje', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group form-check">
                                            {!!form::checkbox('send_message', true, null, ['id' => 'send_message'])!!}
                                            {!! Form::label('send_message', 'Quiere enviar un mensaje?', ['class' => 'form-check-label']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        document.querySelector('#cancel').addEventListener('click', e =>{
            document.location.href = "{{ route('clients.index')}}";
        })
    </script>
@stop
