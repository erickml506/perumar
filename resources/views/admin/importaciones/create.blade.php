@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-sm-12 pb-5">
                <div id="alert" class="alert alert-success alert-dismissible fade d-none" role="alert">
                    <strong>Recurso creado con exito!</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">
                                    <h4>Agregar Importacion</h4>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="email-tab" data-toggle="tab" href="#email" role="tab" aria-controls="email" aria-selected="false">
                                    <h4>Enviar Email</h4>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="create" role="tabpanel" aria-labelledby="create-tab">
                                {!! Form::open(['id' => 'form_create', 'class'=> '_form']) !!}
                                    <div class="form-row">
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Cliente') !!}
                                            {!! Form::select('user_id', $users, null, ['placeholder' => 'Selecciona una opcion','class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Imp') !!}
                                            {!! Form::text('imp', null, ['class' => 'form-control', ]) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('ETA Callao') !!}
                                            {!! Form::date('eta_callao', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Descarga') !!}
                                            {!! Form::date('descarga', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Fac. cliente') !!}
                                            {!! Form::text('fac_client', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Nro. manfiesto') !!}
                                            {!! Form::text('nro_manif', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Booking') !!}
                                            {!! Form::text('booking', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Consignee') !!}
                                            {!! Form::select('consignee', $consignees, null, ['placeholder' => 'Selecciona una opcion',
                                                'class' => 'form-control d-inline',
                                                'id' => 'consignee']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Shipper') !!}
                                            {!! Form::select('shipper',$shippers, null, ['placeholder' => 'Selecciona una opcion',
                                            'class' => 'form-control',
                                            'id' => 'shippers']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('N°MBL') !!}
                                            {!! Form::text('nmbl', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('N°HBL') !!}
                                            {!! Form::text('nhbl', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Cantidad') !!}
                                            {!! Form::number('cantidad', null, ['class' => 'form-control', 'min' => 0, 'onkeydown'=> 'javascript: return event.keyCode == 69 ? false : true']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Logistico') !!}
                                            {!! Form::select('logistico', ['SI'=>'SI','NO'=>'NO'], null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Linea') !!}
                                            {!! Form::select('linea', $lineas, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('MN') !!}
                                            {!! Form::select('mn', $mns, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('POL') !!}
                                            {!! Form::select('pol', $pols, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Pais') !!}
                                            {!! Form::select('pais', $paises, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Puerto de descarga') !!}
                                            {!! Form::select('pod', $pods, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
										</div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Almacen') !!}
                                            {!! Form::select('almacen', $almacenes, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Tipo') !!}
                                            {!! Form::select('tipo', $tipos, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Tamaño') !!}
                                            {!! Form::select('tamanho', $tamaños, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Agentes de carga') !!}
                                            {!! Form::select('agentes_carga', $agentes_cargas, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Agentes de aduana') !!}
                                            {!! Form::select('agentes_aduana', $agentes_aduanas, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-2">
                                            {!! Form::label('DUA') !!}
                                            {!! Form::text('dua', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-6">
                                            {!! Form::label('Canal') !!}
                                            {!! Form::select('canal', $canales, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-6">
                                            {!! Form::label('Tipo de importacion') !!}
                                            {!! Form::select('tipo_import', $tipo_imports, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12">
                                            {!! Form::label('Contenedores') !!}
                                            {!! Form::textarea('contenedores', null, ['class' => 'form-control', 'placeholder' => '- Contenedor 1']) !!}
                                        </div>
                                        {!!form::button('Crear',['id'=>'btn_create','class'=>'btn btn-dark btn-md'])!!}

                                        {!!form::button('Cancelar',['id'=>'cancel','content'=>'Cancelar','class'=>'btn btn-default btn-md'])!!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade" id="email" role="tabpanel" aria-labelledby="email-tab">
                                {!! Form::open(['id' => 'form_send_email']) !!}
                                    <div class="form-row">
                                        <div class="form-group col-sm-12">
                                            {!! Form::label('Email') !!}
                                            {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-12">
                                            {!! Form::label('Mensaje') !!}
                                            {!! Form::textarea('mensaje', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group form-check">
                                            {!!form::checkbox('send_message', true, null, ['id' => 'send_message'])!!}
                                            {!! Form::label('send_message', 'Quiere enviar un mensaje?', ['class' => 'form-check-label']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'id' => 'form_create_something']) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre') !!}
                            {!! Form::text('nombre', null, ['class' => 'form-control','id'=>'name_']) !!}
                        </div>
                        {!! Form::text('type', 'import', ['class' => 'd-none', 'id'=>'type_']) !!}
                    {!! Form::close() !!}
                    <table class="table table-bordered table-hover table-sm" id="table_some">
                        <thead>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>ACCION</th>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="add_" type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')

    <script>
        document.querySelector('#cancel').addEventListener('click', e =>{
            document.location.href = "{{ route('imports.index')}}";
        })

        var withCero = {{$lastRecordId}} < 10 ? '0' : '';
        document.querySelector('input[name=imp]').value = 'IMP' + withCero+'{{$lastRecordId + 1}}-' + (new Date()).getUTCFullYear().toString().substr(-2);
    </script>
@stop
