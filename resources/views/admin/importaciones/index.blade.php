@extends('admin.main')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('css/buttons.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@endsection
@section('content')
    <div class="container-fluid pt-5">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            {{-- <div class="card">
                <div class="card-header"><h2>Buscar</h2></div>
                <div class="card-body">
                    {!! Form::open(['method' => 'GET', 'route' => 'imports.index']) !!}
                        <div class="input-group mb-3">
                            <input name="query" type="text" class="form-control" placeholder="buscar" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Buscar</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    {!! Form::open(['method' => 'GET', 'route' => 'imports.index']) !!}
                        <div class="input-group mb-3">
                             {!! Form::select('orderBy', $attributes, null, ['placeholder' => 'ordernar por', 'class' => 'form-control', "aria-describedby" => "basic-addon2"]) !!}
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Ordenar</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
             </div> --}}
            <div class="card mt-5">
                <div class="card-header"><h2>Importaciones</h2></div>
                <div class="card-body table-responsive">
                    <table id="table-imports" class="table table-bordered table-hover table-sm nowrap">
                        <thead>
                            <th>IMP</th>
                            <th>ETA CALLAO</th>
                            <th>FECHA DE DESCARGA</th>
                            <th>CONSIGNEE</th>
                            <th>SHIPPER</th>
                            <th>BOOKING</th>
                            <th>N°MBL</th>
                            <th>N°HBL</th>
                            <th>NRO MANIF</th>
                            <th>MN</th>
                            <th>CANTIDAD</th>
                            <th>TAMAÑO</th>
                            <th>TIPO</th>
                            <th>CONTENEDORES</th>
                            <th>LINEA</th>
                            <th>PAIS DE ORIGEN</th>
                            <th>POL</th>
                            <th>POD</th>
                            <th>AGENTES DE CARGA</th>
                            <th>LOGISTICO</th>
                            <th>ALMACEN</th>
                            <th>AGENTES DE ADUANA</th>
                            <th>DUA</th>
                            <th>CANAL</th>
                            <th>INCIDENCIAS</th>
                            <th>ESTATUS</th>
                            @if (auth()->user()->permissions !== 'Cliente')
                                <th>ACCIÓN</th>
                            @endif
                        </thead>
                        <tbody>
                            @if($imports->count() > 0)
                                @foreach ($imports as $import)
                                    <tr
                                        @if ($import->status === 'INACTIVO')
                                            style="background-color: #d3d3d378;"
                                        @endif
                                    >
                                        <td>{{$import->imp}}</td>
                                        <td>{{$import->eta_callao}}</td>
                                        <td>{{$import->descarga}}</td>
                                        <td>{{$import->consignee}}</td>
                                        <td>{{$import->shipper}}</td>
                                         <td>{{$import->booking}}</td>
                                        <td>{{$import->nmbl}}</td>
                                        <td>{{$import->nhbl}}</td>
                                        <td>{{$import->nro_manif}}</td>
                                         <td>{{$import->mn}}</td>
                                        <td>{{$import->cantidad}}</td>
                                        <td>{{$import->tamanho}}</td>
                                        <td>{{$import->tipo}}</td>
                                        <td>{{str_replace('-', '', $import->contenedores)}}</td>
                                        <td>{{$import->linea}}</td>
                                        <td>{{$import->pais}}</td>
                                        <td>{{$import->pol}}</td>
                                        <td>{{$import->pod}}</td>
                                        <td>{{$import->agentes_carga}}</td>
                                        <td>{{$import->logistico}}</td>
                                        <td>{{$import->almacen}}</td>
                                        <td>{{$import->agentes_aduana}}</td>
                                        <td>{{$import->dua}}</td>
                                        <td>{{$import->canal}}</td>
                                        <td>{{str_replace('-', '', $import->incidencias)}}</td>
                                        <td>{{ STATUS[$import->status] }}</td>
                                        @if (auth()->user()->permissions !== 'Cliente')
                                        <td class="d-flex">

                                            @if ($import->status === 'ACTIVO')
                                                <a href="{{route('imports.edit', $import->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteImport({{$import->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                                {!! Form::open(['route' => ['imports.destroy', $import->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_import_'.$import->id]) !!}
                                                {!! Form::close() !!}
                                            @else
                                                @if (auth()->user()->permissions !== 'Admin')
                                                    <a href="{{route('imports.edit', $import->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                    <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteImport({{$import->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                                    {!! Form::open(['route' => ['imports.destroy', $import->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_import_'.$import->id]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            @endif



                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan = 25 align="center">No tiene importaciones</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    @if (auth()->user()->permissions !== 'Cliente')
                    <button type="button" id="addimport"  name="addimport" class="btn btn-dark btn-" >Agregar Importación</button>
                     @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function actions(){
            let addBtn = document.querySelector('#addimport');

            addBtn.addEventListener('click', e =>{
                document.location.href = "{{ route('imports.create')}}";
            });
            this.deleteImport = id => {

                confirm('¿Seguro que desea eliminar este registro?')  ? document.forms['delete_import_'+ id].submit() : null;
            }
        }
        actions();
    </script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('js/buttons/buttons.flash.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ asset('js/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/buttons/buttons.print.min.js') }}"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(document).ready(function() {
            var tableImports = $('#table-imports').DataTable({
                dom: 'Bfrtip',
                colReorder: true,
                buttons: [
                    'excel'
                ]
            });

            $('#table-imports tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    tableImports.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
        });
    </script>
@stop
