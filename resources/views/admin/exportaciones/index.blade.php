@extends('admin.main')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('css/buttons.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@endsection
@section('content')
    <div class="container-fluid pt-5">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
             {{-- <div class="card">
                <div class="card-header"><h2>Buscar</h2></div>
                <div class="card-body">
                    {!! Form::open(['method' => 'GET', 'route' => 'exports.index']) !!}
                        <div class="input-group mb-3">
                            <input name="query" type="text" class="form-control" placeholder="buscar" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Button</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
             </div> --}}
            <div class="card mt-5">
                <div class="card-header"><h2>Exportaciones</h2></div>
                <div class="card-body">
                    <table id="table-exports" class="table table-bordered table-hover table-sm nowrap  @if (auth()->user()->permissions !== 'Cliente') table-responsive  @endif">
                        <thead>
                            @if (auth()->user()->permissions !== 'Cliente')
                            <th>EXP</th>
                            <th>ETA</th>
                            <th>RE</th>
                            <th>BOOKING</th>
                            <th>CLIENTE</th>
                            <th>CONSIGNEE</th>
                            <th>REF.CLIENTE</th>
                            <th>N°BL</th>
                            <th>CANTIDAD</th>
                            <th>TAMAÑO</th>
                            <th>LINEA</th>
                            <th>MN</th>
                            <th>NRO MANIFIESTO</th>

                            {{-- <th>PAIS</th>
                            <th>DESTINO</th> --}}
                            <th>NAVE TRANSBORDO</th>
                            <th>PUERTO TRANSBORDO</th>
                            <th>POD</th>
                            <th>DESTINO FINAL</th>
                            <th>FACTURA DEL CLIENTE</th>
                            <th>F. ZARPE</th>
                            <th>AGENTES DE ADUANA</th>
                            <th>AGENTES DE CARGA</th>
                            <th>ALMACEN</th>
                            <th>CONTENEDORES</th>
                            <th>DUA/F.REG</th>
                            <th>DUA RECIBIDA DEL AG. ADUANA</th>
                            <th>FECHA ENTREGA CLIENTE</th>
                            <th>INCIDENCIAS</th>
                             <th>ESTATUS</th>
                            @else
                            <th>REF.CLIENTE</th>
                            <th>CLIENTE</th>
                            <th>BOOKING</th>
                            <th>CONTENEDORES</th>
                            <th>F. ZARPE</th>
                            <th>NAVE TRANSBORDO / FECHA</th>
                            <th>PUERTO TRANSBORDO / FECHA</th>
                            <th>POD / FECHA</th>
                            <th>DESTINO FINAL / FECHA</th>
                            <th>DUA/F.REG</th>
                            <th>FECHA ENTREGA CLIENTE</th>
                            @endif
                            @if (auth()->user()->permissions !== 'Cliente')
                                <th>ACCIÓN</th>
                            @endif
                        </thead>
                        <tbody>
                            @if($exports->count() > 0)
                                @foreach ($exports as $export)
                                    <tr
                                         @if ($export->status === 'INACTIVO')
                                            style="background-color: #d3d3d378;"
                                        @endif
                                    >
                                        @if (auth()->user()->permissions !== 'Cliente')
                                        <td>{{$export->exp}}</td>
                                        <td>{{$export->eta}}</td>
                                        <td>{{$export->re}}</td>
                                        <td>{{$export->booking}}</td>
                                        <td>{{$export->user_id}}</td>
                                        <td>{{$export->consignee}}</td>
                                        <td>{{$export->ref_client}}</td>
                                        <td>{{$export->nbl}}</td>
                                        <td>{{$export->cantidad}}</td>
                                        <td>{{$export->tamanho}}</td>
                                        <td>{{$export->linea}}</td>
                                        <td>{{$export->mn}}</td>
                                        <td>{{$export->nro_manif}}</td>

                                        {{-- <td>{{$export->pais}}</td>
                                        <td>{{$export->destino}}</td> --}}
                                        <td>{{$export->nave_transbordo ? $export->nave_transbordo . ' / ' . $export->nave_transbordo_date : $export->nave_transbordo}}</td>
                                        <td>{{$export->puerto_transbordo ? $export->puerto_transbordo. ' / ' . $export->puerto_transbordo_date : $export->puerto_transbordo}}</td>
                                        <td>{{$export->pod ? $export->pod . ' / ' . $export->pod_date : $export->pod}}</td>
                                        <td>{{$export->destino_final ? $export->destino_final . ' / ' . $export->destino_final_date : $export->destino_final}}</td>
                                        <td>{{$export->fac_client}}</td>
                                        <td>{{$export->fec_zarpe}}</td>
                                        <td>{{$export->agentes_aduana}}</td>
                                        <td>{{$export->agentes_carga}}</td>
                                        <td>{{$export->almacen}}</td>
                                        <td>{{str_replace('-', '', $export->contenedores)}}</td>
                                        <td>{{$export->dua_f_reg}}</td>
                                        <td>{{$export->dua_agent_aduana}}</td>
                                        <td>{{$export->fec_entrega_client}}</td>
                                        <td>{{str_replace('-', '', $export->incidencias)}}</td>
                                        <td>{{ STATUS[$export->status] }}</td>
                                        @else
                                        <td>{{$export->ref_client}}</td>
                                        <td>{{$export->user_id}}</td>
                                        <td>{{$export->booking}}</td>
                                        <td>{{str_replace('-', '', $export->contenedores)}}</td>
                                        <td>{{$export->fec_zarpe}}</td>
                                        <td>{{$export->nave_transbordo ? $export->nave_transbordo . ' / ' . $export->nave_transbordo_date : $export->nave_transbordo}}</td>
                                        <td>{{$export->puerto_transbordo ? $export->puerto_transbordo. ' / ' . $export->puerto_transbordo_date : $export->puerto_transbordo}}</td>
                                        <td>{{$export->pod ? $export->pod . ' / ' . $export->pod_date : $export->pod}}</td>
                                        <td>{{$export->destino_final ? $export->destino_final . ' / ' . $export->destino_final_date : $export->destino_final}}</td>
                                        <td>{{$export->dua_f_reg}}</td>
                                        <td>{{$export->eta}}</td>
                                        <td>{{$export->fec_entrega_client}}</td>

                                        @endif
                                        @if (auth()->user()->permissions !== 'Cliente')
                                        <td class="d-flex">
                                            @if ($export->status === 'ACTIVO')
                                                <a href="{{route('exports.edit', $export->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteExport({{$export->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                                {!! Form::open(['route' => ['exports.destroy', $export->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_export_'.$export->id]) !!}
                                                {!! Form::close() !!}
                                            @else
                                                @if (auth()->user()->permissions !== 'Admin')
                                                    <a href="{{route('exports.edit', $export->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                    <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteExport({{$export->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                                    {!! Form::open(['route' => ['exports.destroy', $export->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_export_'.$export->id]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            @endif
                                        </td>
                                        @endif

                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan = 25 align="center">No tiene exportaciones</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    @if (auth()->user()->permissions !== 'Cliente')
                        <button type="button" id="addexport"  name="addexport" class="btn btn-dark " >Agregar Exportación</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function actions(){
            let addBtn = document.querySelector('#addexport');

            addBtn.addEventListener('click', e =>{
                document.location.href = "{{ route('exports.create')}}";
            });
            this.deleteExport = (id) => {
                confirm('¿Seguro que desea eliminar este registro?') ? document.forms['delete_export_'+ id].submit() : null;
            }
        }
        actions();
    </script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('js/buttons/buttons.flash.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ asset('js/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/buttons/buttons.print.min.js') }}"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $.fn.dataTable.ext.errMode = 'throw';
        $(document).ready(function() {
            var table = $('#table-exports').DataTable({
                dom: 'Bfrtip',
                colReorder: true,
                buttons: [
                    'excel'
                ]
            });

            $('#table-exports tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
        });


    </script>
@stop
