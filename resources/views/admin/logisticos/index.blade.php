@extends('admin.main')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('css/buttons.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@endsection
@section('content')
    <div class="container-fluid pt-5">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card mt-5">
                <div class="card-header"><h2>Logisticos</h2></div>
                <div class="card-body">
                    <table id="table-logistics" class="table table-bordered table-hover table-sm table-responsive nowrap">
                        <thead>
                            <th>SERV.LOG</th>
                            <th>ETA</th>
                            <th>ETD</th>
                            <th>BOOKING</th>
                            <th>MBL</th>
                            <th>CLIENTE</th>
                            <th>SHIPPER</th>
                            <th>CONSIGNEE</th>
                            <th>OPERACION</th>
                            <th>HBL</th>
                            <th>CANTIDAD</th>
                            <th>EQUIPO</th>
                            <th>LINEA</th>
                            <th>NAVE</th>
                            <th>NRO MANIFIESTO</th>
                            <th>DESTINO</th>
                            <th>FACTURA CLIENTE</th>
                            <th>AGENTES ADUANA</th>
                            <th>ALMACEN</th>
                            <th>CONTENEDORES</th>
                            <th>CANAL</th>
                            <th>DUA/F.REG</th>
                            <th>DUA RECIBIDA DEL AG. ADUANA</th>
                            <th>FECHA ENTREGA CLIENTE</th>
                            <th>INCIDENCIAS</th>
                            <th>ESTATUS</th>
                            @if (auth()->user()->permissions !== 'Cliente')
                                <th>ACCIÓN</th>
                            @endif
                        </thead>
                        <tbody>
                            @if($logistics->count() > 0)
                                @foreach ($logistics as $logistic)
                                    <tr
                                        @if ($logistic->status === 'INACTIVO')
                                            style="background-color: #d3d3d378;"
                                        @endif
                                    >
                                        <td>{{$logistic->serv_log}}</td>
                                        <td>{{$logistic->eta}}</td>
                                        <td>{{$logistic->etd}}</td>
                                        <td>{{$logistic->booking}}</td>
                                        <td>{{$logistic->mbl}}</td>
                                        <td>{{$logistic->user->name}}</td>
                                        <td>{{$logistic->shipper}}</td>
                                        <td>{{$logistic->consignee}}</td>
                                        <td>{{$logistic->operation}}</td>
                                        <td>{{$logistic->hbl}}</td>
                                        <td>{{$logistic->cantidad}}</td>
                                        <td>{{$logistic->equipo}}</td>
                                        <td>{{$logistic->linea}}</td>
                                        <td>{{$logistic->nave}}</td>
                                        <td>{{$logistic->nro_manif}}</td>
                                        <td>{{$logistic->destino}}</td>
                                        <td>{{$logistic->fac_client}}</td>
                                        <td>{{$logistic->agentes_aduana}}</td>
                                        <td>{{$logistic->almacen}}</td>
                                        <td>{{str_replace('-', '', $logistic->contenedores)}}</td>
                                        <td>{{$logistic->canal}}</td>
                                        <td>{{$logistic->dua_f_reg}}</td>
                                        <td>{{$logistic->dua_agent_aduana}}</td>
                                        <td>{{$logistic->fec_entrega_client}}</td>
                                        <td>{{str_replace('-', '', $logistic->incidencias)}}</td>
                                        <td>{{ STATUS[$logistic->status] }}</td>
                                        @if (auth()->user()->permissions !== 'Cliente')
                                        <td class="d-flex">

                                             @if ($logistic->status === 'ACTIVO')
                                                <a href="{{route('logistics.edit', $logistic->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteLogistic({{$logistic->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                                {!! Form::open(['route' => ['logistics.destroy', $logistic->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_logistic_'.$logistic->id]) !!}
                                                {!! Form::close() !!}
                                            @else
                                                @if (auth()->user()->permissions !== 'Admin')
                                                    <a href="{{route('logistics.edit', $logistic->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                                                    <a href="#" class="btn btn-danger btn-sm" onclick="(new actions()).deleteLogistic({{$logistic->id}})"  data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                                                    {!! Form::open(['route' => ['logistics.destroy', $logistic->id] , 'method' => 'DELETE', 'class' => 'd-none', 'id' => 'delete_logistic_'.$logistic->id]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            @endif

                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan = 25 align="center">No tiene logisticaciones</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    @if (auth()->user()->permissions !== 'Cliente')
                    <button type="button" id="addlogistic"  name="addlogistic" class="btn btn-dark btn-" >Agregar Logistico</button>
                     @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function actions(){
            let addBtn = document.querySelector('#addlogistic');

            addBtn.addEventListener('click', e =>{
                document.location.href = "{{ route('logistics.create')}}";
            });
            this.deleteLogistic = (id) => {
                confirm('¿Seguro que desea eliminar este registro?') ? document.forms['delete_export_'+ id].submit() : null;
            }
        }
        actions();
    </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('js/buttons/buttons.flash.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ asset('js/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/buttons/buttons.print.min.js') }}"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $.fn.dataTable.ext.errMode = 'throw';
        $(document).ready(function() {
            var table = $('#table-logistics').DataTable({
                dom: 'Bfrtip',
                colReorder: true,
                buttons: [
                    'excel'
                ]
            });

            $('#table-logistics tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
        });
    </script>
@stop
