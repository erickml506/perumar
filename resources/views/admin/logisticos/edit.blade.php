@extends('admin.main')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-sm-12 pb-5">
                <div id="alert" class="alert alert-success alert-dismissible fade d-none" role="alert">
                    <strong>Logistico actualizado con exito!</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">
                                    <h4>Agregar Logistico</h4>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="email-tab" data-toggle="tab" href="#email" role="tab" aria-controls="email" aria-selected="false">
                                    <h4>Enviar Email</h4>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="create" role="tabpanel" aria-labelledby="create-tab">
                                {!! Form::model($logistic, ['route' => ['logistics.update', $logistic], 'method'=>'PUT','class'=> '_form',  'files'=> true, 'id' => 'form_update']) !!}
                                    <div class="form-row">
                                        {!! Form::text('id', null, ['class' => 'd-none', 'id' => 'id']) !!}
                                         <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Serv. log') !!}
                                            {!! Form::text('serv_log', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('ETA') !!}
                                            {!! Form::date('eta', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('ETD') !!}
                                            {!! Form::date('etd', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Booking') !!}
                                            {!! Form::text('booking', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('MBL') !!}
                                            {!! Form::text('mbl', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Cliente') !!}
                                            {!! Form::select('user_id', $users, null, ['placeholder' => 'Selecciona una opcion','class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Shipper') !!}
                                            {!! Form::select('shipper', $shippers, null, ['placeholder' => 'Selecciona una opcion',
                                                'class' => 'form-control d-inline',
                                                'id' => 'shipper']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Consignee') !!}
                                            {!! Form::select('consignee', $consignees, null, ['placeholder' => 'Selecciona una opcion',
                                                'class' => 'form-control d-inline',
                                                'id' => 'consignee']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Operacion') !!}
                                             {!! Form::select('operation', $operations,  null, [ 'placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('HBL') !!}
                                            {!! Form::text('hbl', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Cantidad') !!}
                                            {!! Form::number('cantidad', null, ['class' => 'form-control', 'min' => 0, 'onkeydown'=> 'javascript: return event.keyCode == 69 ? false : true']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Equipo') !!}
                                            {!! Form::text('equipo', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-4 col-md-3">
                                            {!! Form::label('Linea') !!}
                                            {!! Form::select('linea', $lineas, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {!! Form::label('Nave') !!}
                                            {!! Form::text('nave', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {!! Form::label('Nro manifiesto') !!}
                                            {!! Form::text('nro_manif', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-12 col-md-3">
                                            {!! Form::label('Estado') !!}
                                            {!! Form::select('status', STATUS, null, [
                                                'placeholder' => 'Selecciona una opcion',
                                                'class' => 'form-control',
                                                'style' => 'border-color:red'
                                            ]) !!}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {!! Form::label('Destino') !!}
                                            {!! Form::text('destino', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-3">
                                            {!! Form::label('Factura Cliente') !!}
                                            {!! Form::text('fac_client', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-3">
                                            {!! Form::label('Agentes de aduana') !!}
                                             {!! Form::select('agentes_aduana', $agentes_aduanas, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-3">
                                            {!! Form::label('Almacen') !!}
                                            {!! Form::select('almacen', $almacenes, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-3">
                                            {!! Form::label('Canal') !!}
                                            {!! Form::select('canal', $canales, null, ['placeholder' => 'Selecciona una opcion', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-3">
                                            {!! Form::label('DUA/F.REF') !!}
                                            {!! Form::text('dua_f_reg', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-3">
                                            {!! Form::label('DUA recibida del ag. aduana') !!}
                                            {!! Form::date('dua_agent_aduana', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-3">
                                            {!! Form::label('Fecha entrega al cliente') !!}
                                            {!! Form::date('fec_entrega_client', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6">
                                            {!! Form::label('Contenedores') !!}
                                            {!! Form::textarea('contenedores', null, ['class' => 'form-control', 'placeholder' => 'Escriba los contenedores usando - y salto de linea']) !!}
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6">
                                            {!! Form::label('Incidencias') !!}
                                            {!! Form::textarea('incidencias', null, ['class' => 'form-control', 'placeholder' => 'Describe las incidencias usando - y salto de linea']) !!}
                                        </div>
                                        {!!form::button('Editar',['id'=>'btn_update','class'=>'btn btn-dark btn-md d-flex'])!!}

                                        {!!form::button('Cancelar',['id'=>'cancel','content'=>'Cancelar','class'=>'btn btn-default btn-md'])!!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade" id="email" role="tabpanel" aria-labelledby="email-tab">
                                {!! Form::open(['id' => 'form_send_email']) !!}
                                    <div class="form-row">
                                        <div class="form-group col-sm-12">
                                            {!! Form::label('Email') !!}
                                            {!! Form::email('email', $client->email, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group col-sm-12">
                                            {!! Form::label('Mensaje') !!}
                                            {!! Form::textarea('mensaje', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group form-check">
                                            {!!form::checkbox('send_message', true, null, ['id' => 'send_message'])!!}
                                            {!! Form::label('send_message', 'Quiere enviar un mensaje?', ['class' => 'form-check-label']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'id' => 'form_create_something']) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre') !!}
                            {!! Form::text('nombre', null, ['class' => 'form-control','id'=>'name_']) !!}
                        </div>
                        {!! Form::text('type', 'logistic', ['class' => 'd-none', 'id'=>'type_']) !!}
                    {!! Form::close() !!}
                    <table class="table table-bordered table-hover table-sm" id="table_some">
                        <thead>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>ACCION</th>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="add_" type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        document.querySelector('#cancel').addEventListener('click', e =>{
            document.location.href = "{{ route('logistics.index')}}";
        })
    </script>
@stop
