<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Importaciones</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
    <div class="card">
            <div class="card-header"><h2>Importaciones</h2></div>

            @isset($mensaje)
                <p>{{$mensaje}}</p>
            @endisset

            <div class="card-body table-responsive">
                <table class="table table-bordered table-hover table-sm" border="1">
                    <thead>
                        <th>IMP</th>
                        <th>ETA CALLAO</th>
                        <th>FECHA DE DESCARGA</th>
                        <th>CONSIGNEE</th>
                        <th>SHIPPER</th>
                        <th>BOOKING</th>
                        <th>N°MBL</th>
                        <th>N°HBL</th>
                        <th>NRO MANIF</th>
                        <th>MN</th>
                        <th>CANTIDAD</th>
                        <th>TAMAÑO</th>
                        <th>TIPO</th>
                        <th>LINEA</th>
                        <th>PAIS DE ORIGEN</th>
                        <th>POL</th>
                        <th>POD</th>
                        <th>AGENTES DE CARGA</th>
                        <th>LOGISTICO</th>
                        <th>ALMACEN</th>
                        <th>AGENTES DE ADUANA</th>
                        <th>DUA</th>
                        <th>CANAL</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$imp}}</td>
                            <td>{{$eta_callao}}</td>
                            <td>{{$descarga}}</td>
                            <td>{{$consignee}}</td>
                            <td>{{$shipper}}</td>
                            <td>{{$booking}}</td>
                            <td>{{$nmbl}}</td>
                            <td>{{$nhbl}}</td>
                            <td>{{$nro_manif}}</td>
                            <td>{{$mn}}</td>
                            <td>{{$cantidad}}</td>
                            <td>{{$tamanho}}</td>
                            <td>{{$tipo}}</td>
                            <td>{{$linea}}</td>
                            <td>{{$pais}}</td>
                            <td>{{$pol}}</td>
                            <td>{{$pod}}</td>
                            <td>{{$agentes_carga}}</td>
                            <td>{{$logistico}}</td>
                            <td>{{$almacen}}</td>
                            <td>{{$agentes_aduana}}</td>
                            <td>{{$dua}}</td>
                            <td>{{$canal}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @isset($incidencias)
                @php
                    $auxs = explode('-', $incidencias)
                @endphp
                <h3>Incidencias</h3>
                <table border="1">
                    <thead>
                        <th>Incidencias</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($auxs) ; $i++)
                            <tr><td>{{$auxs[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset

            @isset($contenedores)
                @php
                    $conts = explode('-', $contenedores)
                @endphp
                <h3>contenedores</h3>
                <table border="1">
                    <thead>
                        <th>Contenedores</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($conts) ; $i++)
                            <tr><td>{{$conts[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset
        </div>
</body>
</html>
