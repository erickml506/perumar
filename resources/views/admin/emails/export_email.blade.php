<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exportaciones</title>
</head>
<body>
    <div>
            <div><h2>Exportaciones</h2></div>
            @isset($mensaje)
                <p>{{$mensaje}}</p>
            @endisset
            <div class="card-body table-responsive">
                <table class="table table-bordered table-hover table-sm" border="1">
                    <thead>
                        <th>EXP</th>
                        <th>ETA</th>
                        <th>RE</th>
                        <th>BOOKING</th>
                        <th>CLIENTE</th>
                        <th>CONSIGNEE</th>
                        <th>REF.CLIENTE</th>
                        <th>N°BL</th>
                        <th>CANTIDAD</th>
                        <th>TAMAÑO</th>
                        <th>LINEA</th>
                        <th>MN</th>
                        <th>NRO MANIFIESTO</th>

                        {{-- <th>PAIS</th>
                        <th>DESTINO</th> --}}
                        <th>NAVE TRANSBORDO</th>
                        <th>PUERTO TRANSBORDO</th>
                        <th>POD</th>
                        <th>DESTINO FINAL</th>
                        <th>FACTURA DEL CLIENTE</th>
                        <th>F. ZARPE</th>
                        <th>AGENTES DE ADUANA</th>
                        <th>AGENTES DE CARGA</th>
                        <th>ALMACEN</th>
                        <th>DUA/F.REG</th>
                        <th>DUA RECIBIDA DEL AG. ADUANA</th>
                        <th>FECHA ENTREGA CLIENTE</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$exp}}</td>
                            <td>{{$eta}}</td>
                            <td>{{$re}}</td>
                            <td>{{$booking}}</td>
                            <td>{{$user_id}}</td>
                            <td>{{$consignee}}</td>
                            <td>{{$ref_client}}</td>
                            <td>{{$nbl}}</td>
                            <td>{{$cantidad}}</td>
                            <td>{{$tamanho}}</td>
                            <td>{{$linea}}</td>
                            <td>{{$mn}}</td>
                            <td>{{$nro_manif}}</td>

                            {{-- <td>{{$pais}}</td>
                            <td>{{$destino}}</td> --}}
                            <td>{{$nave_transbordo}}</td>
                            <td>{{$puerto_transbordo}}</td>
                            <td>{{$pod}}</td>
                            <td>{{$destino_final}}</td>
                            <td>{{$fac_client}}</td>
                            <td>{{$fec_zarpe}}</td>
                            <td>{{$agentes_aduana}}</td>
                            <td>{{$agentes_carga}}</td>
                            <td>{{$almacen}}</td>
                            <td>{{$dua_f_reg}}</td>
                            <td>{{$dua_agent_aduana}}</td>
                            <td>{{$fec_entrega_client}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @isset($incidencias)
                @php
                    $auxs = explode('-', $incidencias);
                    if(count($auxs) == 1) {
                        $auxs = explode(',', $incidencias);
                    }
                @endphp
                <h3>Incidencias</h3>
                <table border="1">
                    <thead>
                        <th>Incidencias</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($auxs) ; $i++)
                            <tr><td>{{$auxs[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset

            @isset($contenedores)
                @php
                    $conts = explode('-', $contenedores)
                @endphp
                <h3>contenedores</h3>
                <table border="1">
                    <thead>
                        <th>Contenedores</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($conts) ; $i++)
                            <tr><td>{{$conts[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset
        </div>
</body>
</html>
