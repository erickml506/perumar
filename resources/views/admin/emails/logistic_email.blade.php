<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Logisticos</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
    <div class="card">
            <div class="card-header"><h2>Logisticos</h2></div>

            @isset($mensaje)
                <p>{{$mensaje}}</p>
            @endisset

            <div class="card-body table-responsive">
                <table class="table table-bordered table-hover table-sm" border="1">
                    <thead>
                            <th>SERV.LOG</th>
                            <th>ETA</th>
                            <th>ETD</th>
                            <th>BOOKING</th>
                            <th>MBL</th>
                            <th>CLIENTE</th>
                            <th>SHIPPER</th>
                            <th>CONSIGNEE</th>
                            <th>OPERACION</th>
                            <th>HBL</th>
                            <th>CANTIDAD</th>
                            <th>EQUIPO</th>
                            <th>LINEA</th>
                            <th>NAVE</th>
                            <th>NRO MANIFIESTO</th>
                            <th>DESTINO</th>
                            <th>FACTURA CLIENTE</th>
                            <th>AGENTES ADUANA</th>
                            <th>ALMACEN</th>
                            <th>CONTENEDORES</th>
                            <th>CANAL</th>
                            <th>DUA/F.REG</th>
                            <th>DUA RECIBIDA DEL AG. ADUANA</th>
                            <th>FECHA ENTREGA CLIENTE</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$serv_log}}</td>
                            <td>{{$eta}}</td>
                            <td>{{$etd}}</td>
                            <td>{{$booking}}</td>
                            <td>{{$mbl}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$shipper}}</td>
                            <td>{{$consignee}}</td>
                            <td>{{$operation}}</td>
                            <td>{{$hbl}}</td>
                            <td>{{$cantidad}}</td>
                            <td>{{$equipo}}</td>
                            <td>{{$linea}}</td>
                            <td>{{$nave}}</td>
                            <td>{{$nro_manif}}</td>
                            <td>{{$destino}}</td>
                            <td>{{$fac_client}}</td>
                            <td>{{$agentes_aduana}}</td>
                            <td>{{$almacen}}</td>
                            <td>{{$canal}}</td>
                            <td>{{$dua_f_reg}}</td>
                            <td>{{$dua_agent_aduana}}</td>
                            <td>{{$fec_entrega_client}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @isset($incidencias)
                @php
                    $auxs = explode('-', $incidencias)
                @endphp
                <h3>Incidencias</h3>
                <table border="1">
                    <thead>
                        <th>Incidencias</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($auxs) ; $i++)
                            <tr><td>{{$auxs[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset

            @isset($contenedores)
                @php
                    $conts = explode('-', $contenedores)
                @endphp
                <h3>contenedores</h3>
                <table border="1">
                    <thead>
                        <th>Contenedores</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($conts) ; $i++)
                            <tr><td>{{$conts[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset
        </div>
</body>
</html>
