<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exportaciones</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
    <div class="card">
            <div class="card-header"><h2>Clientes</h2></div>

            @isset($mensaje)
                <p>{{$mensaje}}</p>
            @endisset

            <div class="card-body table-responsive">
                <table class="table table-bordered table-hover table-sm" border="1">
                    <thead>
                        <th>NRO EXPORTACION</th>
                        <th>BOOKING</th>
                        <th>VESSEL</th>
                        <th>ETA CALLAO</th>
                        <th>CONSIGNATARIO</th>
                        <th>PUERTO DESTINO</th>
                        <th>CANTIDAD CTNS</th>
                        <th>F. ZARPE</th>
                        <th>PUERTO</th>
                        <th>ETA</th>
                        <th>ETD</th>
                        <th>NAVE TRANSBORDO</th>
                        <th>DESTINO FINAL</th>
                        <th>ETA DESTINO</th>
                        <th>PERUMAR N° EXPORTACION</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$nro_exportacion}}</td>
                            <td>{{$booking}}</td>
                            <td>{{$vessel}}</td>
                            <td>{{$eta_callao}}</td>
                            <td>{{$consignatario}}</td>
                            <td>{{$pod}}</td>
                            <td>{{$cantidad}}</td>
                            <td>{{$fec_zarpe}}</td>
                            <td>{{$puerto}}</td>
                            <td>{{$eta}}</td>
                            <td>{{$etd}}</td>
                            <td>{{$nave}}</td>
                            <td>{{$destino}}</td>
                            <td>{{$eta_destino}}</td>
                            <td>{{$perumar_n_export}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @isset($observaciones)
                @php
                    $auxs = explode('-', $observaciones)
                @endphp
                <h3>Observaciones</h3>
                <table border="1">
                    <thead>
                        <th>Observaciones</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($auxs) ; $i++)
                            <tr><td>{{$auxs[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset

            @isset($contenedores)
                @php
                    $conts = explode('-', $contenedores)
                @endphp
                <h3>contenedores</h3>
                <table border="1">
                    <thead>
                        <th>Contenedores</th>
                    </thead>
                    <tbody>
                        @for ($i =0 ;  $i < count($conts) ; $i++)
                            <tr><td>{{$conts[$i]}}</td></tr>
                        @endfor
                    </tbody>
                </table>
            @endisset
        </div>
</body>
</html>
