<h1>IMPORTACIONES POR VENCER</h1>
 <table border="1">
    <thead>
        <th>IMP</th>
        <th>ETA CALLAO</th>
        <th>FECHA DE DESCARGA</th>
        <th>CONSIGNEE</th>
        <th>SHIPPER</th>
        <th>BOOKING</th>
        <th>N°MBL</th>
        <th>N°HBL</th>
        <th>NRO MANIF</th>
        <th>MN</th>
        <th>CANTIDAD</th>
        <th>TAMAÑO</th>
        <th>TIPO</th>
        <th>CONTENEDORES</th>
        <th>LINEA</th>
        <th>PAIS DE ORIGEN</th>
        <th>POL</th>
        <th>POD</th>
        <th>AGENTES DE CARGA</th>
        <th>LOGISTICO</th>
        <th>ALMACEN</th>
        <th>AGENTES DE ADUANA</th>
        <th>DUA</th>
        <th>CANAL</th>
        <th>INCIDENCIAS</th>
    </thead>
    <tbody>
        @if($imports->count() > 0)
            @foreach ($imports as $import)
                <tr>
                    <td>{{$import->imp}}</td>
                    <td>{{$import->eta_callao}}</td>
                    <td>{{$import->descarga}}</td>
                    <td>{{$import->consignee}}</td>
                    <td>{{$import->shipper}}</td>
                    <td>{{$import->booking}}</td>
                    <td>{{$import->nmbl}}</td>
                    <td>{{$import->nhbl}}</td>
                    <td>{{$import->nro_manif}}</td>
                    <td>{{$import->mn}}</td>
                    <td>{{$import->cantidad}}</td>
                    <td>{{$import->tamanho}}</td>
                    <td>{{$import->tipo}}</td>
                    <td>{{str_replace('-', '', $import->contenedores)}}</td>
                    <td>{{$import->linea}}</td>
                    <td>{{$import->pais}}</td>
                    <td>{{$import->pol}}</td>
                    <td>{{$import->pod}}</td>
                    <td>{{$import->agentes_carga}}</td>
                    <td>{{$import->logistico}}</td>
                    <td>{{$import->almacen}}</td>
                    <td>{{$import->agentes_aduana}}</td>
                    <td>{{$import->dua}}</td>
                    <td>{{$import->canal}}</td>
                    <td>{{str_replace('-', '', $import->incidencias)}}</td>
                </tr>
            @endforeach
        @else
            <tr><td colspan = 25 align="center">Ninguna importacion vence el día de mañana.</td></tr>
        @endif
    </tbody>
</table>
<h1>EXPORTACIONES POR VENCER</h1>
<table border="1">
    <thead>
        <th>EXP</th>
        <th>ETA</th>
        <th>RE</th>
        <th>BOOKING</th>
        <th>CLIENTE</th>
        <th>CONSIGNEE</th>
        <th>REF.CLIENTE</th>
        <th>N°BL</th>
        <th>CANTIDAD</th>
        <th>TAMAÑO</th>
        <th>LINEA</th>
        <th>MN</th>
        <th>NRO MANIFIESTO</th>
        <th>NAVE TRANSBORDO</th>
        <th>PUERTO TRANSBORDO</th>
        <th>POD</th>
        <th>DESTINO FINAL</th>
        <th>FACTURA DEL CLIENTE</th>
        <th>F. ZARPE</th>
        <th>AGENTES DE ADUANA</th>
        <th>AGENTES DE CARGA</th>
        <th>ALMACEN</th>
        <th>CONTENEDORES</th>
        <th>DUA/F.REG</th>
        <th>DUA RECIBIDA DEL AG. ADUANA</th>
        <th>FECHA ENTREGA CLIENTE</th>
        <th>INCIDENCIAS</th>
        <th>BOOKING</th>
        <th>EXP</th>
        <th>CONSIGNEE</th>
        <th>CONTENEDORES</th>
        <th>F. ZARPE</th>
        <th>ETA</th>
        <th>NAVE TRANSBORDO</th>
        <th>DESTINO FINAL</th>
        <th>INCIDENCIAS</th>
    </thead>
    <tbody>
        @if($exports->count() > 0)
            @foreach ($exports as $export)
                <tr>
                    <td>{{$export->exp}}</td>
                    <td>{{$export->eta}}</td>
                    <td>{{$export->re}}</td>
                    <td>{{$export->booking}}</td>
                    <td>{{$export->user_id}}</td>
                    <td>{{$export->consignee}}</td>
                    <td>{{$export->ref_client}}</td>
                    <td>{{$export->nbl}}</td>
                    <td>{{$export->cantidad}}</td>
                    <td>{{$export->tamanho}}</td>
                    <td>{{$export->linea}}</td>
                    <td>{{$export->mn}}</td>
                    <td>{{$export->nro_manif}}</td>
                    <td>{{$export->nave_transbordo ? $export->nave_transbordo . ' / ' . $export->nave_transbordo_date : $export->nave_transbordo}}</td>
                    <td>{{$export->puerto_transbordo ? $export->puerto_transbordo. ' / ' . $export->puerto_transbordo_date : $export->puerto_transbordo}}</td>
                    <td>{{$export->pod ? $export->pod . ' / ' . $export->pod_date : $export->pod}}</td>
                    <td>{{$export->destino_final ? $export->destino_final . ' / ' . $export->destino_final_date : $export->destino_final}}</td>
                    <td>{{$export->fac_client}}</td>
                    <td>{{$export->fec_zarpe}}</td>
                    <td>{{$export->agentes_aduana}}</td>
                    <td>{{$export->agentes_carga}}</td>
                    <td>{{$export->almacen}}</td>
                    <td>{{str_replace('-', '', $export->contenedores)}}</td>
                    <td>{{$export->dua_f_reg}}</td>
                    <td>{{$export->dua_agent_aduana}}</td>
                    <td>{{$export->fec_entrega_client}}</td>
                    <td>{{str_replace('-', '', $export->incidencias)}}</td>
                    <td>{{$export->booking}}</td>
                    <td>{{$export->exp}}</td>
                    <td>{{$export->consignee}}</td>
                    <td>{{str_replace('-', '', $export->contenedores)}}</td>
                    <td>{{$export->fec_zarpe}}</td>
                    <td>{{$export->eta}}</td>
                    <td>{{$export->nave_transbordo}}</td>
                    <td>{{$export->destino_final}}</td>
                    <td>{{str_replace('-', '', $export->incidencias)}}</td>
                </tr>
            @endforeach
        @else
            <tr><td colspan = 25 align="center">Ninguna exportacion vence el día de mañana.</td></tr>
        @endif
    </tbody>
</table>

<h1>LOGISTICOS POR VENCER:</h1>
<table border="1">
    <thead>
        <th>SERV.LOG</th>
        <th>ETA</th>
        <th>ETD</th>
        <th>BOOKING</th>
        <th>MBL</th>
        <th>CLIENTE</th>
        <th>SHIPPER</th>
        <th>CONSIGNEE</th>
        <th>OPERACION</th>
        <th>HBL</th>
        <th>CANTIDAD</th>
        <th>EQUIPO</th>
        <th>LINEA</th>
        <th>NAVE</th>
        <th>NRO MANIFIESTO</th>
        <th>DESTINO</th>
        <th>FACTURA CLIENTE</th>
        <th>AGENTES ADUANA</th>
        <th>ALMACEN</th>
        <th>CONTENEDORES</th>
        <th>CANAL</th>
        <th>DUA/F.REG</th>
        <th>DUA RECIBIDA DEL AG. ADUANA</th>
        <th>FECHA ENTREGA CLIENTE</th>
        <th>INCIDENCIAS</th>
    </thead>
    <tbody>
        @if($logistics->count() > 0)
            @foreach ($logistics as $logistic)
                <tr>
                    <td>{{$logistic->serv_log}}</td>
                    <td>{{$logistic->eta}}</td>
                    <td>{{$logistic->etd}}</td>
                    <td>{{$logistic->booking}}</td>
                    <td>{{$logistic->mbl}}</td>
                    <td>{{$logistic->user->name}}</td>
                    <td>{{$logistic->shipper}}</td>
                    <td>{{$logistic->consignee}}</td>
                    <td>{{$logistic->operation}}</td>
                    <td>{{$logistic->hbl}}</td>
                    <td>{{$logistic->cantidad}}</td>
                    <td>{{$logistic->equipo}}</td>
                    <td>{{$logistic->linea}}</td>
                    <td>{{$logistic->nave}}</td>
                    <td>{{$logistic->nro_manif}}</td>
                    <td>{{$logistic->destino}}</td>
                    <td>{{$logistic->fac_client}}</td>
                    <td>{{$logistic->agentes_aduana}}</td>
                    <td>{{$logistic->almacen}}</td>
                    <td>{{str_replace('-', '', $logistic->contenedores)}}</td>
                    <td>{{$logistic->canal}}</td>
                    <td>{{$logistic->dua_f_reg}}</td>
                    <td>{{$logistic->dua_agent_aduana}}</td>
                    <td>{{$logistic->fec_entrega_client}}</td>
                    <td>{{str_replace('-', '', $logistic->incidencias)}}</td>
                </tr>
            @endforeach
        @else
            <tr><td colspan = 25 align="center">Ninguna logistico por vencer.</td></tr>
        @endif
    </tbody>
</table>

