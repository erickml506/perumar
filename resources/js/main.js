$(function(){
  var $ICON_MINIMIZE = $('#minimize_sidebar')
  var $MENU_SIDEBAR = $('.left_colum')
  var $CONTENT = $('.right_column')
  var $ITEM_MINIMIZE = $('.options_list--item:eq(2)')
  var $MENU_ITEMS_A = $('.menu_list--item a')
  var $MENU_ICONS = $('.menu_list--item i')
  var $BRAN_LOGO = $('#brand--logo')
  var IS_OPEN = true
  var COUNT_RESIZE_TIMES = 0
	var RESIZE_TIMER
	var ACTIVES_SUBMENUS
  var HELPER = {
    hideLogo: () => {
      $BRAN_LOGO.addClass('brand--logo--hide')
      $BRAN_LOGO.text('P').css({'top':'-20px','font-size':'35px','padding-right': '3px', 'text-align':'right'})
    },
    showLogo: () => {
      $BRAN_LOGO.removeClass('brand--logo--hide')
      $BRAN_LOGO.text('PERUMAR').css({'top':'','font-size':'25px','padding-right': '', 'text-align':'center'})
		},
		closeAllSubmenus: () => {
			ACTIVES_SUBMENUS = $('.submenu_list.active')
			ACTIVES_SUBMENUS.removeClass('active')
		},
		openAllsubmenus: () => {
			ACTIVES_SUBMENUS.addClass('active')
		},
    changeIconToExand: () => {
      $ICON_MINIMIZE.removeClass('fa-arrow-left').addClass('fa-arrow-right')
    },
    changeIconToMinimize: () => {
      $ICON_MINIMIZE.removeClass('fa-arrow-right').addClass('fa-arrow-left')
    },
    minimizeMenuOptions: () => {
      $MENU_ITEMS_A.css('visibility', 'hidden')
      $MENU_ICONS.css({'float':'right','right': '15px'})
      $ITEM_MINIMIZE.css('right', '-15px')
    },
    maximizeMenuOptions: () => {
      $MENU_ITEMS_A.css('visibility', 'visible')
      $MENU_ICONS.css({'float':'left','right': ''})
      $ITEM_MINIMIZE.css('right', '0')
    },
    expandContent: () => {
      $CONTENT.css('width', 'calc(100% - 50px)')
    },
    contractContect: () => {
      $CONTENT.css('width', 'calc(100% - 250px)')
    },
    minimizeMenu() {
      this.hideLogo()
      this.changeIconToExand()
			this.minimizeMenuOptions()
			this.closeAllSubmenus()
      $MENU_SIDEBAR .css('left', '-200px')
      this.expandContent()
    },
    maximizeMenu() {
      this.showLogo()
      this.changeIconToMinimize()
			this.maximizeMenuOptions()
			$MENU_SIDEBAR .css('left', '0')
			this.openAllsubmenus()
      this.contractContect()
    }

  }

  $ICON_MINIMIZE.on('click', function(){
    IS_OPEN ? HELPER.minimizeMenu() : HELPER.maximizeMenu()
    IS_OPEN = !IS_OPEN
  })

	$MENU_ITEMS_A.on('click', function () {
		$(this).next('ul').toggleClass('active')
	})

  $(window).on('resize', function(){
    COUNT_RESIZE_TIMES++
    COUNT_RESIZE_TIMES == 1 ? HELPER.minimizeMenu() : null
    IS_OPEN = false
    clearTimeout(RESIZE_TIMER);
    RESIZE_TIMER = setTimeout(function() {
      COUNT_RESIZE_TIMES = 0
    }, 250);
  })

});
