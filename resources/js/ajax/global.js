var globals = {
  module        : $('meta[name=module]') ? $('meta[name=module]').attr('content') : null,
  btn_create    : $('#btn_create'),
  btn_update    : $('#btn_update'),
  btn_cancel    : $('#cancel'),
  frm_create    : $('#form_create'),
  frm_update    : $('#form_update'),
  frm_email     : $('#form_send_email'),
  alert         : $('#alert'),
  id            : $('#id').val(), //resource's id that is editing
  regexEmail    : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  selects       : $('._form select'),
  selectActual  : null,
  btn_add_som   : $('#add_'),
  modal         : $('#modal'),
  resource      : '',
  inputName     : $('#name_'),
  inputType     : $('#type_'),
  frmCreateSome : $('#form_create_something'),
  table_body    : $('#table_some tbody'),
  response      : null
}

window.globals = globals;

export default globals


