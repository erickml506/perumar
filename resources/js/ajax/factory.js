import global from './global'
import globals from './global';

var factory = {
    _makeSingularWord(pluralWord) {
        if ( pluralWord.slice(pluralWord.length - 1) === "s") {
            return pluralWord.slice(0, pluralWord.length -1)
        }
        return pluralWord;
    },
    _blockElement(element) {
        if (element.length > 1) {
            element.forEach(el => {
                el.css({ 'pointer-events': 'none', 'opacity': '.6' })
            })
        } else {
            element.css({ 'pointer-events': 'none', 'opacity': '.6' })
        }
    },
    _unBlockElement(element) {
        if (element.length > 1) {
            element.forEach(el => {
                el.css({ 'pointer-events': '', 'opacity': '1' })
            })
        } else {
            element.css({ 'pointer-events': '', 'opacity': '1' })
        }
    },
    _resetForm(form) {
        if (form.length > 1) {
            form.forEach(el => {
                el[0].reset()
            })
        } else {
            form.reset()
        }
    },
    _beforeAction(action) {
        factory._blockElement([global['btn_' + action], global['btn_cancel']])
        global['btn_' + action].html('<i class="fa fa-spinner fa-pulse fa-custom fa-fw"></i>')
    },
    _afterAction(action) {
        factory._unBlockElement([global['btn_' + action], global['btn_cancel']])
        let textButton = ''
        if(action == 'create') textButton = 'Crear'
        else if(action == 'update') textButton = 'Editar'
        else textButton = 'Guardar'
        global['btn_' + action].html(textButton)
    },
    _successAction(action) {
        global['alert'].removeClass('d-none').addClass('show')
        action == 'create' ? factory._resetForm([global.frm_create, global.frm_email]) : void 0
    },
    _onError() {
        alert('Por favor complete los campos del formulario.')
    },
    _getHttpRequestType(action) {
        if (action.search('create') >= 0 || action.search('add') >= 0) {
            return 'POST'
        } else if (action.search('update') >= 0 || action.search('edit') >= 0) {
            return 'PUT'
        } else if (action.search('delete') >= 0) {
            return 'DELETE'
        } else if (action.search('get') >= 0) {
            return 'GET'
        }

    },
    _ajax: (data, url, action, isButton = true, callback = null) => {
        let type = factory._getHttpRequestType(action);
        let actualPath =  location.href.split(".pre").length > 1 ? '' : '/intranet';
        let options = {
            url: actualPath + url,
            type: type,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': window._token
            },
            timeout: 15000,
            contentType: 'application/json',
            beforeSend: () => {
                if (isButton) {
                    factory._beforeAction(action)
                }
            },
            success: (res, status, xhr) => {
                globals.response = res
                console.log(res)
                if (isButton) {
                    factory._successAction(action)
                }
                if (callback) {
                    callback()
                }

            },
            error: (responseData, textStatus, errorThrown) => {
                console.log(responseData, textStatus)
                factory._onError()
            },
            complete: () => {
                if (isButton) {
                    factory._afterAction(action)
                }

            }
        }
        if (type == 'POST' || type == 'PUT') {
            options.data = JSON.stringify(data)
        }
        $.ajax(options)
    },
    _makeFormBody: () => {
        let body = ``
        globals.response.forEach(item => {
            if (item.type == factory._makeSingularWord(globals.module)) {
                body += `
                    <tr>
                        <td id="td_id">${item.id}</td>
                        <td id="td_name">${item.nombre}</td>
                        <td>
                            <a href="#" class="btn btn-danger btn-sm" data-id="${item.id}" id="delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                            <a href="#" class="btn btn-default btn-sm" id="edit_item"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>`
            }
        })
        globals.table_body.append(body)
    },
    _appendChild: () => {
        globals.table_body.append(`
             <tr>
                <td id="td_id">${globals.response.id}</td>
                <td id="td_name">${globals.response.nombre}</td>
                <td>
                    <a href="#" class="btn btn-danger btn-sm" data-id="${globals.response.id}" id="delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                    <a href="#" class="btn btn-default btn-sm" id="edit_item"><i class="fa fa-edit"></i></a>
                </td>
            </tr>
        `)
        globals.selectActual.append(`
            <option value="${globals.response.id}">${globals.response.nombre}</option>
        `)
    },
    _getFormData: (form) => {
        let UNINDEXDED_ARRAY = []
        let INDEXED_ARRAY = {}
        if (form.length > 1) {
            form.forEach(el => {
                let aux = el.serializeArray()
                aux.forEach((item) => {
                    UNINDEXDED_ARRAY.push(item)
                })
            })
        } else {
            UNINDEXDED_ARRAY = form.serializeArray()
        }
        $.map(UNINDEXDED_ARRAY, n => {
            INDEXED_ARRAY[n['name']] = n['value'];
        })
        return INDEXED_ARRAY
    },
    _hasEmail: () => {
        if ($('#send_message:checked').length == 0)
            return false
        else
            return true
    },
    _isEmailValid(email) {
        return global.regexEmail.test(email)
    },
    _validateAllForm: (dataForm) => {
        let hasError = {}
        for (let key in dataForm) {
            if (dataForm[key].required && dataForm[key] == '') {
                $('input[name=' + key + ']').addClass('is-invalid')
                hasError[key] = key
            } else {
                $('input[name=' + key + ']').removeClass('is-invalid')
                delete hasError[key]
            }
        }
        return Object.value(hasError)
    },
    _validateFormOnInput: (dataForm) => {
        for (let key in dataForm) {
            $('input[name=' + key + ']').on('keydown', function () {
                if (key === 'email') {
                    this._isEmailValid($(this).val())
                }
                $(this).val() == ''
                    ? $(this).hasClass('is-invalid')
                        ? void 0
                        : $(this).addClass('is-invalid')
                    : void 0
            })
        }
    },
    _resetErrors: (form) => {
        $('#' + form + ' .errorForm').each(function () {
            $(this).css('visibility', 'hidden')
        })
    }
}
export default factory
