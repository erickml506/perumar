import globals from './global';
import factory from './factory';


globals.btn_create.on('click', function () {
    let data = factory._hasEmail() ? factory._getFormData([globals.frm_create, globals.frm_email]) : factory._getFormData(globals.frm_create)
    console.log(data);
    factory._ajax(data, `/admin/${globals.module}`, 'create')
})

globals.btn_update.on('click', function () {
    let data = factory._hasEmail() ? factory._getFormData([globals.frm_update, globals.frm_email]) : factory._getFormData(globals.frm_update)
    factory._ajax(data, `/admin/${globals.module}/${globals.id}`, 'update')
})

//SECTION FOR ALL MODULES IN IMPORT

//Capture actual resource
globals.selects.on('change', function () {
    console.log(this.name)
        if (this.value == 1 && this.name != 'user_id') {
            globals.resource = $(this).attr('name')
            globals.selectActual = $(this)
            factory._ajax('', '/admin/' + globals.resource, 'get', false, function () {
                factory._makeFormBody();
                globals.modal.modal('show')
            })

        }
})


//clean modal
globals.modal.on('hidden.bs.modal', function (e) {
    globals.table_body.empty()
    globals.inputName.val('')
    globals.selectActual.val('')
})

//Edit resource
globals.table_body.on('click', '#edit_item', function () {
    $(this).toggleClass('editing')
    if ($(this).hasClass('editing')) {
        let td_element = $(this).parent('td').prev('#td_name')
        let td_element_value = td_element.text()
        td_element.empty()
        td_element.append(`<input type="text" value="${td_element_value}"/>`)
    } else {
        let td_element = $(this).parent('td')
        let item_id = td_element.siblings('#td_id').text()
        let input_element = td_element.prev('#td_name').find('input')
        let input_element_value = input_element.val()
        let data = {nombre: input_element_value, type: factory._makeSingularWord(globals.module)}
        factory._ajax(data, '/admin/' + globals.resource + '/' + item_id, 'update', false, function(){
            td_element.prev('#td_name').empty()
            td_element.prev('#td_name').text(input_element_value)
            globals.selectActual.find(`option[value=${item_id}]`).text(input_element_value)
        })

    }
})

//Delete resource
globals.table_body.on('click', '#delete_item', function () {
    let context = $(this)
    let id = context.attr('data-id')
    let wantDeleted = confirm('¿Estás seguro de elimar este registro?')
    if (wantDeleted) {
        factory._ajax('', '/admin/' + globals.resource + '/' + id, 'delete', false, function () {
            context.parents('tr').remove()
            globals.selectActual.find(`option[value=${id}]`).remove()
        })
    }
})

globals.btn_add_som.on('click', function () {
    let data = factory._getFormData(globals.frmCreateSome)
    factory._ajax(data, '/admin/' + globals.resource, 'add_som', true, function () {
        factory._appendChild()
    })
})
