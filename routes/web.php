<?php
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();
Route::get('register', 'Auth\LoginController@showLoginForm');
Route::get('password/reset', 'Auth\LoginController@showLoginForm')->name('password.request');
Route::get('/admin', 'HomeController@index')->name('admin');

Route::prefix('admin')->group(function () {
    Route::get('/notify:email', function () {
        Artisan::call('notify:email');
        return redirect()->route('admin')->with('status', 'Se enviaron los correo con los registros pior vencer, correctamente.');
    });
    Route::resource('imports', 'ImportController');
    Route::resource('exports', 'ExportController');
    Route::resource('logistics', 'LogisticController');
    Route::resource('clients', 'ClientController');
    Route::resource('users', 'UserController');
    Route::resource('shipper', 'ShipperController');
    Route::resource('consignee', 'ConsigneeController');
    Route::resource('linea', 'LineaController');
    Route::resource('mn', 'MnController');
    Route::resource('pol', 'PolController');
    Route::resource('pais', 'PaisController');
    Route::resource('pod', 'PodController');
    Route::resource('almacen', 'AlmacenController');
    Route::resource('tipo', 'TipoController');
    Route::resource('tamanho', 'TamañoController');
    Route::resource('agentes_carga', 'AgenteCargaController');
    Route::resource('agentes_aduana', 'AgenteAduanaController');
    Route::resource('canal', 'CanalController');
    Route::resource('tipo_import', 'TipoImportController');
    Route::resource('destino', 'DestinoController');
    Route::resource('destino_final', 'DestinoFinalController');
    Route::resource('puerto_transbordo', 'PuertoTransbordoController');
    Route::resource('client', 'ClienteController');
    Route::resource('operation', 'OperationController');
    Route::resource('nave_transbordo', 'NaveTransbordoController');
});


Route::get('/test', function () {

});