<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Tamaño extends Model
{
    protected $table = 'tamaños';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
