<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'paises';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
