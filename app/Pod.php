<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Pod extends Model
{
    protected $table = 'pods';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
