<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;
use Perumar\Helpers\UrlHelper;

class Import extends Model
{
    protected $table = 'imports';
    protected $fillable = [
        'imp',
        'user_id',
        'eta_callao',
        'descarga',
        'fac_client',
        'nro_manif',
        'booking',
        'consignee',
        'shipper',
        'nmbl',
        'nhbl',
        'cantidad',
        'tamanho',
        'logistico',
        'tipo',
        'tipo_import',
        'linea',
        'mn',
        'pol',
        'pais',
        'pod',
        'almacen',
        'contenedores',
        'agentes_carga',
        'agentes_aduana',
        'dua',
        'canal',
        'status',
        'incidencias'
    ];

    public function attributes()
    {
        return $this->fillable;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getTipoImportAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $tipo_import = TipoImport::find($value);
            return $tipo_import->nombre ?? null;
        }
        return $value;
    }

    public function getTamanhoAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $tamaño = Tamaño::find($value);
            return $tamaño->nombre ?? null;
        }
        return $value;
    }

    public function getConsigneeAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $consignee = Consignee::find($value);
            return $consignee->nombre ?? null;
        }
        return $value;
    }

    public function getShipperAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $shipper = Shipper::find($value);
            return $shipper->nombre ?? null;
        }
        return $value;
    }

    public function getTipoAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $tipo = Tipo::find($value);
            return $tipo->nombre ?? null;
        }
        return $value;
    }

    public function getLineaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $linea = Linea::find($value);
            return $linea->nombre ?? null;
        }
        return $value;
    }

    public function getMnAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $mn = Mn::find($value);
            return $mn->nombre ?? null;
        }
        return $value;
    }

    public function getPaisAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $pais = Pais::find($value);
            return $pais->nombre ?? null;
        }
        return $value;
    }

    public function getPolAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $pol = Pol::find($value);
            return $pol->nombre ?? null;
        }
        return $value;
    }

    public function getAlmacenAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $almacen = Almacen::find($value);
            return $almacen->nombre ?? null;
        }
        return $value;
    }

    public function getPodAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $pod = Pod::find($value);
            return $pod->nombre ?? null;
        }
        return $value;
    }

    public function getAgentesCargaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $agentes_carga = AgenteCarga::find($value);
            return $agentes_carga->nombre ?? null;
        }
        return $value;
    }

    public function getAgentesAduanaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $agentes_aduana = AgenteAduana::find($value);
            return $agentes_aduana->nombre ?? null;
        }
        return $value;
    }

    public function getCanalAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $canal = Canal::find($value);
            return $canal->nombre ?? null;
        }
        return $value;
    }
}
