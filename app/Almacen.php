<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
    protected $table = 'almacenes';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
