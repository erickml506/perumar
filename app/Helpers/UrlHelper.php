<?php

namespace Perumar\Helpers;

class UrlHelper {

    /**
     * Verify if can mutate values in a model
     *
     * @return boolean
     */
    public static function canMutateValueIn($module)
    {
        if ( static::isCreateRoute($module) ||  static::isEditRoute() || static::isUpdateRoute()){
            return false;
        }
        return true;
    }
    /**
     * Verify if is a edit route
     *
     * @return boolean
     */
    public static function isEditRoute()
    {
        if (request()->method() == 'GET') {
            $arrRoute = explode('/', request()->path());
            return count($arrRoute) === 4 ?  is_numeric($arrRoute[2]) : false;
        }
        return false;

    }
    /**
     * Verify if is a create route
     *
     * @return boolean
     */
    public static function isCreateRoute($module)
    {
        if (request()->method() == 'POST' && request()->path() == "admin/$module") {
            return !static::isSendingEmail();
        }
        return false;
    }
    /**
     * verify if is update route
     *
     * @return boolean
     */
    public static function isUpdateRoute()
    {
        if (request()->method() == 'PUT') {
            $arrRoute = explode('/', request()->path());
            return count($arrRoute) === 4 ?  is_numeric($arrRoute[2]) : false;
        }
        return false;
    }
    /**
     * Verify if is sending mail
     *
     * @return boolean
     */
    static function isSendingEmail()
    {
        $message = request()->post('send_message');
        $status = isset($message) ?? false;
        return $status;
    }
}