<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class NaveTransbordo extends Model
{
    protected $table = 'nave_transbordos';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
