<?php

namespace Perumar;

use Perumar\Helpers\UrlHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Cache;

class Export extends Model
{
    public $today;
    protected $table = 'exports';
    protected $fillable = [
        're',
        'exp',
        'eta',
        'booking',
        'user_id',
        'consignee',
        'ref_client',
        'nbl',
        'cantidad',
        'tamanho',
        'linea',
        'mn',
        'nro_manif',
        'pod', //antes destino
        'pod_date',
        //'pais', //se agrego
        //'destino',
        'destino_final',
        'destino_final_date',
        'puerto_transbordo',
        'puerto_transbordo_date',
        'nave_transbordo',
        'nave_transbordo_date',
        'fac_client',
        'fec_zarpe',
        'agentes_aduana',
        'agentes_carga',
        'almacen',
        'contenedores',
        'dua_f_reg',
        'dua_agent_aduana', //DUA RECIBIDA POR AG. ADUANA
        'fec_entrega_client',
        'status',
        'incidencias'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->today = Carbon::now()->format('Y-m-d');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUserIdAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "user_id_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $user = Cache::get($key);
            } else {
                $user = User::find($value);
                Cache::put($key, $user, now()->addHours(8));
            }
            return $user ? $user->name : null;
        }
        return $value;
    }

    public function getNaveTransbordoAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "nave_transbordo_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $nave = Cache::get($key);
            } else {
                $nave = NaveTransbordo::find($value);
                Cache::put($key, $nave, now()->addHours(8));
            }

            return $nave ? $nave->nombre : null;
        }
        return $value;
    }
    // public function getClientAttribute($value)
    // {
    //     if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
    //         $cliente = Cliente::find($value);
    //         return $cliente ? $cliente->nombre : null;
    //     }
    //     return $value;
    // }

    // public function getPaisAttribute($value)
    // {
    //     if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
    //         $pais = Pais::find($value);
    //         return $pais ? $pais->nombre : null;
    //     }
    //     return $value;
    // }
    public function getPodAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {

            $key =  "pod_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $pod = Cache::get($key);
            } else {
                $pod = Pod::find($value);
                Cache::put($key, $pod, now()->addHours(8));
            }

            return $pod ? $pod->nombre : null;
        }
        return $value;
    }
    public function getPuertoTransbordoAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "puerto_transbordo_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $puertoTransbordo = Cache::get($key);
            } else {
                $puertoTransbordo = PuertoTransbordo::find($value);
                Cache::put($key, $puertoTransbordo, now()->addHours(8));
            }

            return $puertoTransbordo ? $puertoTransbordo->nombre : null;
        }
        return $value;
    }
    public function getDestinoFinalAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "destino_final_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $destinoFinal = Cache::get($key);
            } else {
                $destinoFinal = DestinoFinal::find($value);
                Cache::put($key, $destinoFinal, now()->addHours(8));
            }

            return $destinoFinal ? $destinoFinal->nombre : null;
        }
        return $value;
    }
    /**
     * Get Name instead id
     *
     * @param int $value
     * @return string
     */
    public function getDestinoAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "destino_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $destino = Cache::get($key);
            } else {
                $destino = Destino::find($value);
                Cache::put($key, $destino, now()->addHours(8));
            }

            return $destino ? $destino->nombre : null;
        }
        return $value;
    }
    public function getConsigneeAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "consignee_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $consignee = Cache::get($key);
            } else {
                $consignee = Consignee::find($value);
                Cache::put($key, $consignee, now()->addHours(8));
            }


            return $consignee ? $consignee->nombre : null;
        }
        return $value;
    }

    public function getTamanhoAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "tamanho_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $tamaño = Cache::get($key);
            } else {
                $tamaño = Tamaño::find($value);
                Cache::put($key, $tamaño, now()->addHours(8));
            }

            return $tamaño ? $tamaño->nombre : null;
        }
        return $value;
    }

    public function getLineaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "linea_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $linea = Cache::get($key);
            } else {
                $linea = Linea::find($value);
                Cache::put($key, $linea, now()->addHours(8));
            }


            return $linea->nombre ?? null;
        }
        return $value;
    }

    public function getMnAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "mn_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $mn = Cache::get($key);
            } else {
                $mn = Mn::find($value);
                Cache::put($key, $mn, now()->addHours(8));
            }

            return $mn->nombre ?? null;
        }
        return $value;
    }

    public function getAlmacenAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "almacen_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $almacen = Cache::get($key);
            } else {
                $almacen = Almacen::find($value);
                Cache::put($key, $almacen, now()->addHours(8));
            }

            return $almacen->nombre ?? null;
        }
        return $value;
    }

    public function getAgentesCargaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "agentes_carga_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $agentes_carga = Cache::get($key);
            } else {
                $agentes_carga = AgenteCarga::find($value);
                Cache::put($key, $agentes_carga, now()->addHours(8));
            }

            return $agentes_carga->nombre ?? null;
        }
        return $value;
    }

    public function getAgentesAduanaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $key =  "agentes_aduana_$value" . "_" . $this->today;

            if (Cache::has($key)) {
                $agentes_aduana = Cache::get($key);
            } else {
                $agentes_aduana = AgenteAduana::find($value);
                Cache::put($key, $agentes_aduana, now()->addHours(8));
            }

            return $agentes_aduana->nombre ?? null;
        }
        return $value;
    }
}
