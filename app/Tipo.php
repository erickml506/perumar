<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table = 'tipos';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
