<?php

namespace Perumar;

use Perumar\Helpers\UrlHelper;
use Illuminate\Database\Eloquent\Model;

class Logistic extends Model
{
    protected $table = 'logistics';
    protected $fillable = [
        'serv_log',
        'eta',
        'etd',
        'booking',
        'mbl',
        'client',
        'consignee',
        'shipper',
        'operation',
        'user_id',
        'hbl',
        'cantidad',
        'equipo',
        'linea',
        'nave',
        'nro_manif',
        'destino',
        'fac_client',
        'agentes_aduana',
        'almacen',
        'contenedores',
        'canal',
        'dua_f_reg',
        'dua_agent_aduana', //DUA RECIBIDA POR AG. ADUANA
        'fec_entrega_client',
        'status',
        'incidencias'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getOperationAttribute($value) {
         if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $operation = Operation::find($value);
            return $operation->nombre ?? null;
        }
        return $value;
    }

    public function getShipperAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $shipper = Shipper::find($value);
            return $shipper->nombre ?? null;
        }
        return $value;
    }

    public function getConsigneeAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $consignee = Consignee::find($value);
            return $consignee ? $consignee->nombre : null;
        }
        return $value;
    }

    public function getLineaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $linea = Linea::find($value);
            return $linea ? $linea->nombre : null;
        }
        return $value;
    }

    public function getAgentesAduanaAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $agentes_aduana = AgenteAduana::find($value);
            return $agentes_aduana ? $agentes_aduana->nombre : null;
        }
        return $value;
    }


    public function getAlmacenAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $almacen = Almacen::find($value);
            return $almacen ? $almacen->nombre : null;
        }
        return $value;
    }

    public function getCanalAttribute($value)
    {
        if (UrlHelper::canMutateValueIn($this->table) && is_numeric($value)) {
            $canal = Canal::find($value);
            return $canal ? $canal->nombre : null;
        }
        return $value;
    }
}
