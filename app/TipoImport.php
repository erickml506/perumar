<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class TipoImport extends Model
{
    protected $table = 'tipo_imports';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
