<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class PuertoTransbordo extends Model
{
    protected $table = 'puerto_transbordos';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
