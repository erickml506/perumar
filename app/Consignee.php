<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Consignee extends Model
{
    protected $table = 'consignees';
    protected $fillable = [
        'nombre',
        'type'
    ];
}