<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    protected $table = 'operations';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
