<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Linea extends Model
{
    protected $table = 'lineas';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
