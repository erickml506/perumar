<?php

namespace Perumar\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Perumar\Mail\CronDaily;

class CronEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails every days at 8:00 am';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::to([
            'operaciones1@perumar.com.pe',
            'operaciones2@perumar.com.pe',
            'operaciones3@perumar.com.pe',
            'gromero@perumar.com.pe',
            'erickml506@gmail.com'
        ])->send(new CronDaily());
    }
}
