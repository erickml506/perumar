<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Mn extends Model
{
    protected $table = 'mns';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
