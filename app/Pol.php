<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Pol extends Model
{
    protected $table = 'pols';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
