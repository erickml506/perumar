<?php

namespace Perumar\Http\Controllers;

use Perumar\User;
use Perumar\Canal;
use Perumar\Linea;
use Perumar\Almacen;
use Perumar\Cliente;
use Perumar\Shipper;
use Perumar\Logistic;
use Perumar\Consignee;
use Perumar\Operation;
use Perumar\AgenteAduana;
use Illuminate\Http\Request;
use Perumar\Mail\LogisticMail;
use Illuminate\Support\Facades\Mail;
use DB;

class LogisticController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->permissions !== 'Cliente') {
            $logistics = Logistic::all();
        } else {
            $logistics =  Logistic::where('user_id', auth()->user()->id)->get();
        }

        return view('admin.logisticos.index', compact('logistics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('logistics.index')->with('status', 'No tienes permisos suficientes');
        }
        $shippers = Shipper::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $consignees = Consignee::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $lineas = Linea::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $agentes_aduanas = AgenteAduana::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $almacenes = Almacen::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $canales = Canal::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $clientes = Cliente::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $operations = Operation::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');

        $statement = DB::select("SHOW TABLE STATUS LIKE 'logistics'");
        $nextId = $statement[0]->Auto_increment;

        $lastRecordId = $nextId;
        $users = User::where('permissions', 'Cliente')->pluck('name', 'id');
        $module = 'logistics';
        return view('admin.logisticos.create', compact('shippers', 'consignees', 'lineas', 'agentes_aduanas', 'almacenes', 'canales', 'module', 'lastRecordId', 'clientes', 'operations', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('logistics.index')->with('status', 'No tienes permisos suficientes');
        }
        $logistic = Logistic::create($request->all());
        if (isset($request->send_message)) {
            $logistic['mensaje'] = $request->mensaje;
            Mail::to($request->email)->send(new LogisticMail($logistic));
        }
        return response()->json($logistic);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Logistic  $logistic
     * @return \Illuminate\Http\Response
     */
    public function edit(Logistic $logistic)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('logistics.index')->with('status', 'No tienes permisos suficientes');
        }
        $client = User::find($logistic->user_id);
        $shippers = Shipper::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $consignees = Consignee::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $lineas = Linea::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $agentes_aduanas = AgenteAduana::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $almacenes = Almacen::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $canales = Canal::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $clientes = Cliente::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $operations = Operation::whereIn('type', ['general', 'logistic'])->pluck('nombre', 'id');
        $users = User::where('permissions', 'Cliente')->pluck('name', 'id');
        $module = 'logistics';
        return view('admin.logisticos.edit', compact('client','logistic', 'shippers', 'consignees', 'lineas', 'agentes_aduanas', 'almacenes', 'canales', 'module', 'clientes', 'operations', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Logistic  $logistic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logistic  $logistic)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('logistics.index')->with('status', 'No tienes permisos suficientes');
        }
        $logistic->fill($request->all());
        $logistic->save();
        if (isset($request->send_message)) {
            $logistic['mensaje'] = $request->mensaje;
            Mail::to($request->email)->send(new LogisticMail($logistic));
        }
        return response()->json($logistic);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Logistic  $logistic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logistic  $logistic)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('logistics.index')->with('status', 'No tienes permisos suficientes');
        }

        if (auth()->user()->permissions == 'Admin' && $logistic->status === 'INACTIVO') {
            return redirect()->route('logistics.index')->with('status', 'Registro CONCLUIDO, solo un Superadmin puede eliminar este registro.');
        }

        $logistic->delete();
        return redirect()->route('logistics.index')->with('status', 'Logistico eliminado con exito!');
    }
}
