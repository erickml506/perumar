<?php

namespace Perumar\Http\Controllers;

use Perumar\DestinoFinal;
use Illuminate\Http\Request;

class DestinoFinalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destinosFinales = DestinoFinal::all();
        return response()->json($destinosFinales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destinoFinal = DestinoFinal::create($request->all());
        return response()->json($destinoFinal);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\DestinoFinal  $destinoFinal
     * @return \Illuminate\Http\Response
     */
    public function show(DestinoFinal $destinoFinal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\DestinoFinal  $destinoFinal
     * @return \Illuminate\Http\Response
     */
    public function edit(DestinoFinal $destinoFinal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\DestinoFinal  $destinoFinal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DestinoFinal $destinoFinal)
    {
        $destinoFinal->fill($request->all());
        $destinoFinal->save();
        return response()->json($destinoFinal);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\DestinoFinal  $destinoFinal
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestinoFinal $destinoFinal)
    {
        $destinoFinal->delete();
        return response()->json(['message' => 'success']);
    }
}
