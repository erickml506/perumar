<?php

namespace Perumar\Http\Controllers;

use Perumar\TipoImport;
use Illuminate\Http\Request;

class TipoImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo_imports = TipoImport::all();
        return response()->json($tipo_imports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipo = TipoImport::create($request->all());
        return response()->json($tipo);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\TipoImport  $tipoImport
     * @return \Illuminate\Http\Response
     */
    public function show(TipoImport $tipoImport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\TipoImport  $tipoImport
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoImport $tipoImport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\TipoImport  $tipoImport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoImport $tipoImport)
    {
        $tipoImport->fill($request->all());
        $tipoImport->save();
        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\TipoImport  $tipoImport
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoImport $tipoImport)
    {
        $tipoImport->delete();
        return response()->json(['message' => 'succes'], 200);
    }
}
