<?php

namespace Perumar\Http\Controllers;

use Perumar\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('admin')->with('status', 'No tienes permisos suficientes');
        }
        $users = User::paginate(10);
        return view('admin.usuarios.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('admin')->with('status', 'No tienes permisos suficientes');
        }
        return view('admin.usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('admin')->with('status', 'No tienes permisos suficientes');
        }
        $user = new User($request->all());
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->route('users.index')->with('status', 'success')
            ->with('message', 'Usuario creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('admin')->with('status', 'No tienes permisos suficientes');
        }
        return view('admin.usuarios.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('admin')->with('status', 'No tienes permisos suficientes');
        }
        $user->fill($request->except('password'));
        $request->password ? $user->password = Hash::make($request->password) : null;
        $user->save();
        return redirect()->route('users.index')->with('status', 'success')
            ->with('message', 'Usuario actualizado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('admin')->with('status', 'No tienes permisos suficientes');
        }
        $user->delete();
        return redirect()->route('users.index')->with('status', 'success')
            ->with('message', 'Usuario eliminado con exito!');
    }
}
