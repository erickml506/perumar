<?php

namespace Perumar\Http\Controllers;

use Perumar\Pod;
use Illuminate\Http\Request;

class PodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $pods = Pod::all();
        return response()->json($pods);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pod = Pod::create($request->all());
        return response()->json($pod);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\Pod  $pod
     * @return \Illuminate\Http\Response
     */
    public function show(Pod $pod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\Pod  $pod
     * @return \Illuminate\Http\Response
     */
    public function edit(Pod $pod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\Pod  $pod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pod $pod)
    {
         $pod->fill($request->all());
        $pod->save();
        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\Pod  $pod
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pod $pod)
    {
         $pod->delete();
        return response()->json(['message' => 'succes'], 200);
    }
}
