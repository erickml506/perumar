<?php

namespace Perumar\Http\Controllers;

use Perumar\PuertoTransbordo;
use Illuminate\Http\Request;

class PuertoTransbordoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $puertoTransbordos = PuertoTransbordo::all();
        return response()->json($puertoTransbordos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $puertoTransbordo  = PuertoTransbordo::create($request->all());
        return response()->json($puertoTransbordo);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\PuertoTransbordo  $puertoTransbordo
     * @return \Illuminate\Http\Response
     */
    public function show(PuertoTransbordo $puertoTransbordo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\PuertoTransbordo  $puertoTransbordo
     * @return \Illuminate\Http\Response
     */
    public function edit(PuertoTransbordo $puertoTransbordo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\PuertoTransbordo  $puertoTransbordo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PuertoTransbordo $puertoTransbordo)
    {
        $puertoTransbordo->fill($request->all());
        $puertoTransbordo->save();
        return response()->json($puertoTransbordo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\PuertoTransbordo  $puertoTransbordo
     * @return \Illuminate\Http\Response
     */
    public function destroy(PuertoTransbordo $puertoTransbordo)
    {
        $puertoTransbordo->delete();
        return response()->json($puertoTransbordo);
    }
}
