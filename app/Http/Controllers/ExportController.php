<?php

namespace Perumar\Http\Controllers;

use Perumar\Mn;
use Perumar\Pod;
use Perumar\Pais;
use Perumar\User;
use Perumar\Linea;
use Perumar\Export;
use Perumar\Tamaño;
use Perumar\Almacen;
use Perumar\Cliente;
use Perumar\Destino;
use Perumar\Consignee;
use Perumar\AgenteCarga;
use Perumar\AgenteAduana;
use Perumar\DestinoFinal;
use Perumar\NaveTransbordo;
use Illuminate\Http\Request;
use Perumar\Mail\ExportMail;
use Perumar\PuertoTransbordo;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function index()
    {
        $today = Carbon::now()->format('Y-m-d');
        $key =  "exports_$today";
        if (auth()->user()->permissions !== 'Cliente') {

            if (Cache::has($key)) {
                $exports = Cache::get($key);
            } else {
                $exports = Export::orderBy('exp', 'asc')->get();
                Cache::put($key, $exports, now()->addHours(8));
            }

        } else {
              if (Cache::has($key)) {
                $exports = Cache::get($key);
            } else {
                $exports = Export::where('user_id', auth()->user()->id)->orderBy('exp', 'asc')->get();
                Cache::put($key, $exports, now()->addHours(2));
            }

        }

        return view('admin.exportaciones.index', compact('exports'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('exports.index')->with('status', 'No tienes permisos suficientes');
        }

        $consignees = Consignee::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $tamaños = Tamaño::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $lineas = Linea::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $mns = Mn::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $agentes_cargas = AgenteCarga::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $agentes_aduanas = AgenteAduana::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $almacenes = Almacen::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        //$paises = Pais::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $pods = Pod::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        //$destinos = Destino::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $destinos_finales = DestinoFinal::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $puertos_transbordos = PuertoTransbordo::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $users = User::where('permissions', 'Cliente')->pluck('name', 'id');
        $module = 'exports';
        $naves_transbordos = NaveTransbordo::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');

        $statement = DB::select("SHOW TABLE STATUS LIKE 'exports'");
        $nextId = $statement[0]->Auto_increment;

        $lastRecordId = $nextId;
        return view('admin.exportaciones.create', compact('naves_transbordos', 'puertos_transbordos', 'destinos_finales', 'pods', 'consignees', 'tamaños', 'lineas', 'mns', 'agentes_cargas', 'agentes_aduanas', 'almacenes', 'module', 'users', 'lastRecordId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('exports.index')->with('status', 'No tienes permisos suficientes');
        }
        // $request->validate([
        //     'user_id' => 'required'
        // ]);

        $export = Export::create($request->all());
        if (isset($request->send_message)) {
            $export['mensaje'] = $request->mensaje;
            Mail::to($request->email)->send(new ExportMail($export));
        }
        return response()->json($export);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Export  $export
     * @return \Illuminate\Http\Response
     */
    public function edit(Export $export)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('exports.index')->with('status', 'No tienes permisos suficientes');
        }
        $client = User::find($export->user_id);
        $consignees = Consignee::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $tamaños = Tamaño::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $lineas = Linea::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $mns = Mn::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $agentes_cargas = AgenteCarga::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $agentes_aduanas = AgenteAduana::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $almacenes = Almacen::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        //$paises = Pais::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $pods = Pod::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        //$destinos = Destino::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $destinos_finales = DestinoFinal::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $puertos_transbordos = PuertoTransbordo::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        $users = User::where('permissions', 'Cliente')->pluck('name', 'id');
        $module = 'exports';
        $naves_transbordos = NaveTransbordo::whereIn('type', ['general', 'export'])->pluck('nombre', 'id');
        return view('admin.exportaciones.edit', compact('client','naves_transbordos', 'puertos_transbordos', 'destinos_finales', 'pods', 'export', 'consignees', 'tamaños', 'lineas', 'mns', 'agentes_cargas', 'agentes_aduanas', 'almacenes', 'module', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Export  $export
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Export $export)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('exports.index')->with('status', 'No tienes permisos suficientes');
        }
        $export->fill($request->all());
        $export->save();
        if (isset($request->send_message)) {
            $export['mensaje'] = $request->mensaje;
            Mail::to($request->email)->send(new ExportMail($export));
        }
        return response()->json($export);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Export  $export
     * @return \Illuminate\Http\Response
     */
    public function destroy(Export $export)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('exports.index')->with('status', 'No tienes permisos suficientes');
        }

        if (auth()->user()->permissions == 'Admin' && $export->status === 'INACTIVO') {
            return redirect()->route('exports.index')->with('status', 'Registro CONCLUIDO, solo un Superadmin puede eliminar este registro.');
        }

        $export->delete();
        return redirect()->route('exports.index')->with('status', 'Exportación eliminada con exito!');
    }
}
