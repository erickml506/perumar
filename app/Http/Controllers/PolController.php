<?php

namespace Perumar\Http\Controllers;

use Perumar\Pol;
use Illuminate\Http\Request;

class PolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pols = Pol::all();
        return response()->json($pols);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $pol = Pol::create($request->all());
        return response()->json($pol);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\Pol  $pol
     * @return \Illuminate\Http\Response
     */
    public function show(Pol $pol)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\Pol  $pol
     * @return \Illuminate\Http\Response
     */
    public function edit(Pol $pol)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\Pol  $pol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pol $pol)
    {
        $pol->fill($request->all());
        $pol->save();
        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\Pol  $pol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pol $pol)
    {
         $pol->delete();
        return response()->json(['message' => 'succes'], 200);
    }
}
