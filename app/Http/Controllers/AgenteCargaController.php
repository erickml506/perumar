<?php

namespace Perumar\Http\Controllers;

use Perumar\AgenteCarga;
use Illuminate\Http\Request;

class AgenteCargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agenteCargas = AgenteCarga::all();
        return response()->json($agenteCargas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agenteCarga = AgenteCarga::create($request->all());
        return response()->json($agenteCarga);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\AgenteCarga  $agenteCarga
     * @return \Illuminate\Http\Response
     */
    public function show(AgenteCarga $agenteCarga)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\AgenteCarga  $agenteCarga
     * @return \Illuminate\Http\Response
     */
    public function edit(AgenteCarga $agenteCarga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\AgenteCarga  $agenteCarga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agenteCarga = AgenteCarga::find($id);
        $agenteCarga->fill($request->all());
        $agenteCarga->save();
        return response()->json($agenteCarga);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\AgenteCarga  $agenteCarga
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agenteCarga = AgenteCarga::find($id);
        $agenteCarga->delete();
        return response()->json(['message' => 'succes'], 200);
    }
}
