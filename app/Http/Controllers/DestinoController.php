<?php

namespace Perumar\Http\Controllers;

use Perumar\Destino;
use Illuminate\Http\Request;

class DestinoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destinos = Destino::all();
        return response()->json($destinos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destino = Destino::create($request->all());
        return response()->json($destino);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\Destino  $destino
     * @return \Illuminate\Http\Response
     */
    public function show(Destino $destino)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\Destino  $destino
     * @return \Illuminate\Http\Response
     */
    public function edit(Destino $destino)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\Destino  $destino
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Destino $destino)
    {
        $destino->fill($request->all());
        $destino->save();
        return response()->json($destino);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\Destino  $destino
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destino $destino)
    {
        $destino->delete();
        return response()->json(['message' => 'success']);
    }
}
