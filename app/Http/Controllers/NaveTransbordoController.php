<?php

namespace Perumar\Http\Controllers;

use Perumar\NaveTransbordo;
use Illuminate\Http\Request;

class NaveTransbordoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nave_tranbordos = NaveTransbordo::all();
        return response()->json( $nave_tranbordos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nave_tranbordo = NaveTransbordo::create($request->all());
        return response()->json($nave_tranbordo);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\NaveTransbordo  $naveTransbordo
     * @return \Illuminate\Http\Response
     */
    public function show(NaveTransbordo $naveTransbordo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\NaveTransbordo  $naveTransbordo
     * @return \Illuminate\Http\Response
     */
    public function edit(NaveTransbordo $naveTransbordo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\NaveTransbordo  $naveTransbordo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NaveTransbordo $naveTransbordo)
    {
        $naveTransbordo->fill($request->all());
        $naveTransbordo->save();
        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\NaveTransbordo  $naveTransbordo
     * @return \Illuminate\Http\Response
     */
    public function destroy(NaveTransbordo $naveTransbordo)
    {
        $naveTransbordo->delete();
        return response()->json(['message' => 'succes'], 200);
    }
}
