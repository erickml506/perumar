<?php

namespace Perumar\Http\Controllers;

use Perumar\AgenteAduana;
use Illuminate\Http\Request;

class AgenteAduanaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agenteAduanas = AgenteAduana::all();
        return response()->json($agenteAduanas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agenteAduana = AgenteAduana::create($request->all());
        return response()->json($agenteAduana);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\AgenteAduana  $agenteAduana
     * @return \Illuminate\Http\Response
     */
    public function show(AgenteAduana $agenteAduana)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\AgenteAduana  $agenteAduana
     * @return \Illuminate\Http\Response
     */
    public function edit(AgenteAduana $agenteAduana)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\AgenteAduana  $agenteAduana
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agenteAduana = AgenteAduana::find($id);
        $agenteAduana->fill($request->all());
        $agenteAduana->save();
        return response()->json($agenteAduana);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\AgenteAduana  $agenteAduana
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agenteAduana = AgenteAduana::find($id);
        $agenteAduana->delete();
        return response()->json(['message' => 'success'], 200);
    }
}
