<?php

namespace Perumar\Http\Controllers;

use Perumar\Mn;
use Illuminate\Http\Request;

class MnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mns = Mn::all();
        return response()->json($mns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $mn = Mn::create($request->all());
        return response()->json($mn);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\Mn  $mn
     * @return \Illuminate\Http\Response
     */
    public function show(Mn $mn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\Mn  $mn
     * @return \Illuminate\Http\Response
     */
    public function edit(Mn $mn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\Mn  $mn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mn $mn)
    {
        $mn->fill($request->all());
        $mn->save();
        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\Mn  $mn
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mn $mn)
    {
        $mn->delete();
        return response()->json(['message' => 'succes'], 200);


    }
}
