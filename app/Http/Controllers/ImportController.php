<?php

namespace Perumar\Http\Controllers;

use Perumar\Mn;
use Perumar\Pod;
use Perumar\Pol;
use Perumar\Pais;
use Perumar\Tipo;
use Perumar\User;
use Perumar\Canal;
use Perumar\Linea;
use Perumar\Import;
use Perumar\Tamaño;
use Perumar\Almacen;
use Perumar\Shipper;
use Perumar\Consignee;
use Perumar\TipoImport;
use Perumar\AgenteCarga;
use Perumar\AgenteAduana;
use Illuminate\Http\Request;
use Perumar\Mail\ImportEmail;
use Illuminate\Support\Facades\Mail;
use DB;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->permissions !== 'Cliente') {
            $imports =  Import::all();
        } else {
            $imports = Import::where('user_id', auth()->user()->id)->get();
        }

        return view('admin.importaciones.index', compact('imports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('imports.index')->with('status', 'No tienes permisos suficientes');
        }
        $shippers = Shipper::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $consignees = Consignee::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $lineas = Linea::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $mns = Mn::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $paises = Pais::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $pols = Pol::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $pods = Pod::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $almacenes = Almacen::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $tipos = Tipo::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $tipo_imports = TipoImport::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $tamaños = Tamaño::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $agentes_cargas = AgenteCarga::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $agentes_aduanas = AgenteAduana::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $canales = Canal::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $users = User::where('permissions', 'Cliente')->pluck('name', 'id');
        $module = 'imports';

        $statement = DB::select("SHOW TABLE STATUS LIKE 'imports'");
        $nextId = $statement[0]->Auto_increment;

        $lastRecordId = $nextId;
        return view('admin.importaciones.create',  compact('shippers', 'consignees', 'lineas', 'mns', 'paises', 'pols', 'pods', 'almacenes', 'tipos', 'tamaños', 'agentes_cargas', 'agentes_aduanas', 'canales', 'tipo_imports', 'module', 'users', 'lastRecordId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('imports.index')->with('status', 'No tienes permisos suficientes');
        }
        $import = Import::create($request->all());
        if (isset($request->send_message)) {
            $import['mensaje'] = $request->mensaje;
            Mail::to($request->email)->send(new ImportEmail($import));
        }
        return response()->json($import);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Perumar\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function show(Import $import)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Perumar\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function edit(Import $import)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('imports.index')->with('status', 'No tienes permisos suficientes');
        }
        $client = User::find($import->user_id);
        $shippers = Shipper::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $consignees = Consignee::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $lineas = Linea::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $mns = Mn::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $paises = Pais::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $pols = Pol::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $pods = Pod::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $almacenes = Almacen::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $tipos = Tipo::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $tipo_imports = TipoImport::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $tamaños = Tamaño::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $agentes_cargas = AgenteCarga::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $agentes_aduanas = AgenteAduana::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $canales = Canal::whereIn('type', ['general', 'import'])->pluck('nombre', 'id');
        $users = User::where('permissions', 'Cliente')->pluck('name', 'id');
        $module = 'imports';
        return view('admin.importaciones.edit', compact('import','client', 'shippers', 'consignees', 'lineas', 'mns', 'paises', 'pols', 'pods', 'almacenes', 'tipos', 'tamaños', 'agentes_cargas', 'agentes_aduanas', 'canales', 'tipo_imports', 'module', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Perumar\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Import $import)
    {
        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('imports.index')->with('status', 'No tienes permisos suficientes');
        }
        $import->fill($request->all());
        $import->save();
        if (isset($request->send_message)) {
            $import['mensaje'] = $request->mensaje;
            Mail::to($request->email)->send(new ImportEmail($import));
        }
        return response()->json($import->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Perumar\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function destroy(Import $import)
    {

        if (auth()->user()->permissions == 'Cliente') {
            return redirect()->route('imports.index')->with('status', 'No tienes permisos suficientes');
        }

        if (auth()->user()->permissions == 'Admin' && $import->status === 'INACTIVO') {
            return redirect()->route('imports.index')->with('status', 'Registro CONCLUIDO, solo un Superadmin puede eliminar este registro.');
        }

        $import->delete();
        return redirect()->route('imports.index')->with('status', 'Importacion eliminada con exito!');
    }
}
