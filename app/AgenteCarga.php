<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class AgenteCarga extends Model
{
    protected $table = 'agente_cargas';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
