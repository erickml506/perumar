<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $fillable = [
        'nro_exportacion',
        'booking',
        'vessel',
        'eta_callao',
        'consignatario',
        'pod',
        'cantidad',
        'fec_zarpe',
        'puerto',
        'eta',
        'etd',
        'nave',
        'destino',
        'eta_destino',
        'perumar_n_export',
        'contenedores',
        'status',
        'observaciones'
    ];
}
