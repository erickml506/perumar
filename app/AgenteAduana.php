<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class AgenteAduana extends Model
{
    protected $table = 'agente_aduanas';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
