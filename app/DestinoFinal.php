<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class DestinoFinal extends Model
{
    protected $table = 'destino_finals';
    protected $fillable = [
        'nombre',
        'type'
    ];
}
