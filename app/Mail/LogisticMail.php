<?php

namespace Perumar\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Perumar\Logistic;

class LogisticMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Logistic $logistic)
    {
        $this->logistic = $logistic;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('PERUMAR - NOTIFICACIÓN')->view('admin.emails.logistic_email')
            ->with($this->logistic->toArray());
    }
}
