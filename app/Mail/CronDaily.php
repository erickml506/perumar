<?php

namespace Perumar\Mail;

use Perumar\Export;
use Perumar\Import;
use Perumar\Logistic;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CronDaily extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $nextDay = Carbon::now()->addDay(1)->toDateString();
        $now = Carbon::now()->toDateString();
        $exports = Export::where('status', 'ACTIVO')
        ->where(function ($q) use ($nextDay, $now) {
            $q->whereDate('pod_date', '>=', $now);
            $q->whereDate('pod_date', '<=', $nextDay);
        })
        ->orWhere(function ($q) use ($nextDay, $now) {
            $q->whereDate('nave_transbordo_date', '>=', $now);
            $q->whereDate('nave_transbordo_date', '<=', $nextDay);
        })
        ->orWhere(function ($q) use ($nextDay, $now) {
            $q->whereDate('destino_final_date', '>=', $now);
            $q->whereDate('destino_final_date', '<=', $nextDay);
        })
         ->orWhere(function ($q) use ($nextDay, $now) {
            $q->whereDate('puerto_transbordo_date', '>=', $now);
            $q->whereDate('puerto_transbordo_date', '<=', $nextDay);
        })
        ->get();

        $imports = Import::where('status', 'ACTIVO')
        ->where(function ($q) use ($nextDay, $now) {
            $q->whereDate('eta_callao', '>=', $now);
            $q->whereDate('eta_callao', '<=', $nextDay);
        })
        ->orWhere(function ($q) use ($nextDay, $now) {
            $q->whereDate('descarga', '>=', $now);
            $q->whereDate('descarga', '<=', $nextDay);
        })
        ->get();

        $logistics = Logistic::where('status', 'ACTIVO')
        ->where(function ($q) use ($nextDay, $now) {
            $q->whereDate('fec_entrega_client', '>=', $now);
            $q->whereDate('fec_entrega_client', '<=', $nextDay);
        })
        ->get();

        if ($imports->count() === 0 && $exports->count() === 0 && $logistics->count() === 0) {
            return;
        }

        return $this->subject('PERUMAR - NOTIFICACIÓN')->view('admin.emails.cron_daily', compact('exports', 'logistics', 'imports'));
    }
}
