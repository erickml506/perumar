<?php

namespace Perumar;

use Illuminate\Database\Eloquent\Model;

class Shipper extends Model
{
    protected $table = 'shippers';
    protected $fillable = [
        'nombre',
        'type'
    ];
}