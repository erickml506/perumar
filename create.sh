if [[ -z "$1" ]]; then
    echo "Name of model its required"
    exit 1
fi

if [[ -z "$2" ]]; then
    echo "The ends of the model its required"
    exit 1
fi

echo "Creating Model $1"

php artisan make:model $1 -r -m
echo "Creating Seeder"
php artisan make:seeder "$1$2TableSeeder"