<?php

use Illuminate\Database\Seeder;
use Perumar\Operation;

class OperationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $operations = [
            ['nombre' => 'Agregar Operación', 'type' => 'general'],

        ];
        foreach ($operations as $operation) {
            Operation::create($operation);
        }
    }
}
