<?php

use Illuminate\Database\Seeder;
use Perumar\Shipper;

class ShippersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shippers = [
            ['nombre' => 'Agregar Shipper', 'type' => 'general'],
            ['nombre' => 'DAVID BROWN SANTASALO SOUTH AMERICA', 'type' => 'import'],
            ['nombre' => 'ROAD MACHINERY LLC', 'type' => 'import'],
            ['nombre' => 'EMPIRE SOUTHWEST LLC', 'type' => 'import'],
            ['nombre' => 'IRON PLANET', 'type' => 'import'],
            ['nombre' => 'GENEMCO', 'type' => 'import'],

            ['nombre' => 'WORLD PRODUCTS', 'type' => 'logistic'],
            ['nombre' => 'SUD AMERICAN LUMBER', 'type' => 'logistic'],
            ['nombre' => 'PROVEF', 'type' => 'logistic'],
            ['nombre' => 'SATELLITE', 'type' => 'logistic'],
            ['nombre' => 'SURPACK', 'type' => 'logistic'],
            ['nombre' => 'SERENA MARBLE', 'type' => 'logistic']
        ];
        foreach ($shippers as $shipper) {
            Shipper::create($shipper);
        }
    }
}
