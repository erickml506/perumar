<?php

use Illuminate\Database\Seeder;
use Perumar\PuertoTransbordo;

class PuertoTransbordosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $puertos = [
            ['nombre' => 'Agregar Puerto transbordo', 'type' => 'general']
        ];
        foreach ($puertos as $puerto) {
            PuertoTransbordo::create($puerto);
        }
    }
}
