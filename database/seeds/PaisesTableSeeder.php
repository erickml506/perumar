<?php

use Perumar\Pais;
use Illuminate\Database\Seeder;

class PaisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paises = [
            ['nombre' => 'Agregar Pais', 'type' => 'general'],
            ['nombre' => 'ITALIA', 'type' => 'import'],
            ['nombre' => 'USA', 'type' => 'import'],
            ['nombre' => 'ECUADOR', 'type' => 'import'],
            ['nombre' => 'COLOMBIA', 'type' => 'import'],
            ['nombre' => 'PANAMA', 'type' => 'import'],
            ['nombre' => 'MEXICO', 'type' => 'import']
        ];
        foreach ($paises as $pais) {
            Pais::create($pais);
        }
    }
}
