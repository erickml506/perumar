<?php

use Illuminate\Database\Seeder;
use Perumar\DestinoFinal;

class DestinoFinalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $destinos = [
            ['nombre' => 'Agregar Destino Final', 'type' => 'general']
        ];
        foreach ($destinos as $destino) {
            DestinoFinal::create($destino);
        }
    }
}
