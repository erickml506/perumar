<?php

use Illuminate\Database\Seeder;
use Perumar\Destino;

class DestinosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $destinos = [
            ['nombre' => 'Agregar Destino', 'type' => 'general']
        ];
        foreach ($destinos as $destino) {
            Destino::create($destino);
        }
    }
}
