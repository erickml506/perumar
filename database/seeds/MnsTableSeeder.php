<?php

use Perumar\Mn;
use Illuminate\Database\Seeder;

class MnsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Mns = [
            ['nombre' => 'Agregar Mn', 'type' => 'general'],
            ['nombre' => 'TORONTO', 'type' => 'import'],
            ['nombre' => 'ARICA EXPRESS', 'type' => 'import'],
            ['nombre' => 'SAN VICENTE EXPRESS', 'type' => 'import'],
            ['nombre' => 'MSC NITYA', 'type' => 'import'],
            ['nombre' => 'LOUISIANA TRADER', 'type' => 'import'],
            ['nombre' => 'NORDIC BEIJING', 'type' => 'import'],
            ['nombre' => 'CAPE CHRONOS', 'type' => 'import']
        ];
        foreach ($Mns as $Mn) {
            Mn::create($Mn);
        }
    }
}
