<?php

use Perumar\Canal;
use Illuminate\Database\Seeder;

class CanalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $canales = [
            ['nombre' => 'Agregar Canal', 'type' => 'general'],
            ['nombre' => 'VERDE', 'type' => 'import'],
            ['nombre' => 'NARANJA', 'type' => 'import'],
            ['nombre' => 'ROJO', 'type' => 'import'],

            ['nombre' => 'VERDE', 'type' => 'logistic'],
            ['nombre' => 'NARANJA', 'type' => 'logistic'],
            ['nombre' => 'ROJO', 'type' => 'logistic']
        ];
        foreach ($canales as $canal) {
            Canal::create($canal);
        }
    }
}
