<?php

use Illuminate\Database\Seeder;
use Perumar\AgenteAduana;

class AgenteAduanasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agentes_aduanas = [
            ['nombre' => 'Agregar agente', 'type' => 'general'],
            ['nombre' => 'LAVALLE', 'type' => 'import'],
            ['nombre' => 'AGESIL', 'type' => 'import'],

            ['nombre' => 'LAVALLE', 'type' => 'logistic'],
            ['nombre' => 'AGESIL', 'type' => 'logistic'],
        ];
        foreach ($agentes_aduanas as $agente_aduana) {
            AgenteAduana::create($agente_aduana);
        }
    }
}
