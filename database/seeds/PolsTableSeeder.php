<?php

use Perumar\Pol;
use Illuminate\Database\Seeder;

class PolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Pols = [
            ['nombre' => 'Agregar Pol', 'type' => 'general'],
            ['nombre' => 'GENOVA', 'type' => 'import'],
            ['nombre' => 'MIAMI', 'type' => 'import'],
            ['nombre' => 'LOS ANGELES', 'type' => 'import'],
            ['nombre' => 'NEW YORK', 'type' => 'import'],
            ['nombre' => 'GALVESTON', 'type' => 'import'],
            ['nombre' => 'GUAYAQUIL', 'type' => 'import'],
            ['nombre' => 'BUENAVENTURA', 'type' => 'import'],
            ['nombre' => 'CARTAGENA', 'type' => 'import'],
            ['nombre' => 'BALBOA', 'type' => 'import'],
            ['nombre' => 'COLON', 'type' => 'import'],
            ['nombre' => 'MANZANILLO', 'type' => 'import'],
            ['nombre' => 'LAZARO CARDENAS', 'type' => 'import']
        ];
        foreach ($Pols as $Pol) {
            Pol::create($Pol);
        }
    }
}
