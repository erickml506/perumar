<?php

use Illuminate\Database\Seeder;
use Perumar\Consignee;

class ConsigneesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $consignes = [
            ['nombre' => 'Agregar Consignee', 'type' => 'general'],
            ['nombre' => 'DAVID BROWN SANTASALO PERU', 'type' => 'import'],
            ['nombre' => 'SAN MARTIN CONTRATISTAS GENERALES', 'type' => 'import'],
            ['nombre' => 'FULL RENTAL', 'type' => 'import'],
            ['nombre' => 'RENOVATECH', 'type' => 'import'],
            ['nombre' => 'GRUPO SAN FRANCISCO DE ASIS', 'type' => 'import'],

            ['nombre' => 'AGUA INMACULADA', 'type' => 'logistic'],
            ['nombre' => 'BANTAM', 'type' => 'logistic'],
            ['nombre' => 'DISTRIBUIDORA LATINO ANDINA SL', 'type' => 'logistic'],
            ['nombre' => 'REPROPLASTIC CANADA', 'type' => 'logistic'],
            ['nombre' => 'PROTECTPAK LLC', 'type' => 'logistic'],
            ['nombre' => 'STONEXCHANGE INC.', 'type' => 'logistic']
        ];
        foreach ($consignes as $consigne) {
            Consignee::create($consigne);
        }
    }
}
