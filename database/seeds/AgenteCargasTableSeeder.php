<?php

use Illuminate\Database\Seeder;
use Perumar\AgenteCarga;

class AgenteCargasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agentes_cargas = [
            ['nombre' => 'Agregar agente', 'type' => 'general'],
            ['nombre' => 'UNI USA', 'type' => 'import'],
            ['nombre' => 'UNI CHILE', 'type' => 'import'],
            ['nombre' => 'SYSTEM', 'type' => 'import'],
        ];
        foreach ($agentes_cargas as $agentes_carga) {
            AgenteCarga::create($agentes_carga);
        }
    }
}
