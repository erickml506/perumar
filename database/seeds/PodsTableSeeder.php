<?php

use Illuminate\Database\Seeder;
use Perumar\Pod;

class PodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           $pods = [
                ['nombre' => 'Agregar Pod', 'type' => 'general'],
                ['nombre' => 'CALLAO', 'type' => 'import'],
                ['nombre' => 'PAITA', 'type' => 'import'],
                ['nombre' => 'ILO', 'type' => 'import']
        ];
        foreach ($pods as $pod) {
            Pod::create($pod);
        }
    }
}
