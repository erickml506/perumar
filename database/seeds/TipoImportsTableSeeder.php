<?php

use Illuminate\Database\Seeder;
use Perumar\TipoImport;

class TipoImportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = [
            ['nombre' => 'Agregar Tipo', 'type' => 'general'],
            ['nombre' => 'SADA', 'type' => 'import'],
            ['nombre' => 'EXCEPCIONAL', 'type' => 'import'],
            ['nombre' => 'URGENTE', 'type' => 'import'],
        ];
        foreach ($tipos as $tipo) {
            TipoImport::create($tipo);
        }
    }
}
