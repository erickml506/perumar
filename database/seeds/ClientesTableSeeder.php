<?php

use Illuminate\Database\Seeder;
use Perumar\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientes = [
            ['nombre' => 'Agregar Cliente', 'type' => 'general']
        ];
        foreach ($clientes as $cliente) {
            Cliente::create($cliente);
        }
    }
}
