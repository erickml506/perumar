<?php

use Perumar\Linea;
use Illuminate\Database\Seeder;

class LineasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lineas = [
            ['nombre' => 'Agregar Linea', 'type' => 'general'],
            ['nombre' => 'HSUD', 'type' => 'import'],
            ['nombre' => 'HAPAG LLOYD', 'type' => 'import'],
            ['nombre' => 'MSC', 'type' => 'import'],
            ['nombre' => 'CMA CGM', 'type' => 'import'],
            ['nombre' => 'HYUNDAI', 'type' => 'import'],
            ['nombre' => 'CITIKOLD', 'type' => 'import'],
            ['nombre' => 'MOL', 'type' => 'import'],
            ['nombre' => 'BROOM', 'type' => 'import'],

            ['nombre' => 'HSUD', 'type' => 'logistic'],
            ['nombre' => 'HAPAG LLOYD', 'type' => 'logistic'],
            ['nombre' => 'MSC', 'type' => 'logistic'],
            ['nombre' => 'CMA CGM', 'type' => 'logistic'],
            ['nombre' => 'HYUNDAI', 'type' => 'logistic'],
            ['nombre' => 'CITIKOLD', 'type' => 'logistic'],
            ['nombre' => 'MOL', 'type' => 'logistic'],
            ['nombre' => 'BROOM', 'type' => 'logistic']
        ];
        foreach($lineas as $linea){
            Linea::create( $linea);
        }

    }
}
