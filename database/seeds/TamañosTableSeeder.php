<?php

use Illuminate\Database\Seeder;
use Perumar\Tamaño;

class TamañosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tamaños = [
            ['nombre' => 'Agregar Tamaño', 'type' => 'general'],
            ['nombre' => '40', 'type' => 'import'],
            ['nombre' => '20', 'type' => 'import'],
        ];
        foreach ($tamaños as $tamaño) {
            Tamaño::create($tamaño);
        }
    }
}
