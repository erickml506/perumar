<?php

use Illuminate\Database\Seeder;
use Perumar\Tipo;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = [
            ['nombre' => 'Agregar Tipo', 'type' => 'general'],
            ['nombre' => 'HC', 'type' => 'import'],
            ['nombre' => 'ST', 'type' => 'import'],
            ['nombre' => 'FR', 'type' => 'import'],
            ['nombre' => 'RH', 'type' => 'import'],

            ['nombre' => 'HC', 'type' => 'logistic'],
            ['nombre' => 'ST', 'type' => 'logistic'],
            ['nombre' => 'FR', 'type' => 'logistic'],
            ['nombre' => 'RH', 'type' => 'logistic']
        ];
        foreach ($tipos as $tipo) {
            Tipo::create($tipo);
        }
    }
}
