<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConsigneesTableSeeder::class);
        $this->call(LineasTableSeeder::class);
        $this->call(ShippersTableSeeder::class);
        $this->call(MnsTableSeeder::class);
        $this->call(PolsTableSeeder::class);
        $this->call(PaisesTableSeeder::class);
        $this->call(PodsTableSeeder::class);
        $this->call(AlmacenesTableSeeder::class);
        $this->call(TiposTableSeeder::class);
        $this->call(TamañosTableSeeder::class);
        $this->call(AgenteCargasTableSeeder::class);
        $this->call(AgenteAduanasTableSeeder::class);
        $this->call(CanalesTableSeeder::class);
        $this->call(TipoImportsTableSeeder::class);
        $this->call(PuertoTransbordosTableSeeder::class);
        $this->call(DestinoFinalsTableSeeder::class);
        $this->call(DestinosTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(OperationsTableSeeder::class);
        $this->call(NaveTransbordoTableSeeder::class);
    }
}
