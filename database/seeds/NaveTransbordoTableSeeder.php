<?php

use Illuminate\Database\Seeder;
use Perumar\NaveTransbordo;

class NaveTransbordoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $naves = [
            ['nombre' => 'Agregar Nave tranbordo', 'type' => 'general']
        ];
        foreach ($naves as $nave) {
            NaveTransbordo::create($nave);
        }
    }
}
