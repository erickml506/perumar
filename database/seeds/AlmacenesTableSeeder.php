<?php

use Illuminate\Database\Seeder;
use Perumar\Almacen;

class AlmacenesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $almacenes = [
            ['nombre' => 'Agregar Almacen', 'type' => 'general'],
            ['nombre' => 'DPW', 'type' => 'import'],
            ['nombre' => 'APM', 'type' => 'import'],
            ['nombre' => 'RANSA', 'type' => 'import'],

            ['nombre' => 'DPW', 'type' => 'logistic'],
            ['nombre' => 'APM', 'type' => 'logistic'],
            ['nombre' => 'RANSA', 'type' => 'logistic'],
        ];
        foreach ($almacenes as $almacen) {
            Almacen::create($almacen);
        }
    }
}
