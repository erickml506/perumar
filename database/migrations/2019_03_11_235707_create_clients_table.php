<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nro_exportacion');
            $table->string('booking')->nullable();
            $table->string('vessel')->nullable();
            $table->string('eta_callao')->nullable();
            $table->string('consignatario')->nullable();
            $table->string('pod')->nullable();
            $table->integer('cantidad')->nullable();
            $table->date('fec_zarpe')->nullable();
            $table->string('puerto')->nullable();
            $table->string('eta')->nullable();
            $table->string('etd')->nullable();
            $table->string('nave')->nullable();
            $table->string('destino')->nullable();
            $table->string('eta_destino')->nullable();
            $table->string('perumar_n_export')->nullable();
            $table->text('contenedores')->nullable();
            $table->text('observaciones')->nullable();
            $table->enum('status', ['ACTIVO', 'INACTIVO'])->default('ACTIVO')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
