<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('re');
            $table->string('exp')->nullable();
            $table->date('eta')->nullable();
            $table->string('booking')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('consignee')->nullable();
            $table->string('ref_client')->nullable();
            $table->string('nbl')->nullable();
            $table->integer('cantidad')->nullable();
            $table->string('tamanho')->nullable();
            $table->string('linea')->nullable();
            $table->string('mn')->nullable();
            $table->string('nro_manif')->nullable();
            $table->string('pod')->nullable();
            $table->date('pod_date')->nullable();
            //$table->string('pais')->nullable();
            //$table->string('destino')->nullable();
            $table->string('nave_transbordo')->nullable();
            $table->date('nave_transbordo_date')->nullable();
            $table->string('destino_final')->nullable();
            $table->date('destino_final_date')->nullable();
            $table->string('puerto_transbordo')->nullable();
            $table->date('puerto_transbordo_date')->nullable();
            $table->string('fac_client')->nullable();
            $table->date('fec_zarpe')->nullable();
            $table->string('agentes_carga')->nullable();
            $table->string('agentes_aduana')->nullable();
            $table->string('almacen')->nullable();
            $table->text('contenedores')->nullable();
            $table->string('dua_f_reg')->nullable();
            $table->string('dua_agent_aduana')->nullable();
            $table->date('fec_entrega_client')->nullable();
            $table->text('incidencias')->nullable();
            $table->enum('status', ['ACTIVO', 'INACTIVO'])->default('ACTIVO')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exports');
    }
}
