<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logistics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serv_log');
            $table->date('eta')->nullable();
            $table->date('etd')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('booking')->nullable();
            $table->string('mbl')->nullable();
            $table->string('client')->nullable();
            $table->string('consignee')->nullable();
            $table->string('shipper')->nullable();
            $table->string('operation')->nullable();
            $table->string('hbl')->nullable();
            $table->integer('cantidad')->nullable();
            $table->string('equipo')->nullable();
            $table->string('linea')->nullable();
            $table->string('nave')->nullable();
            $table->string('nro_manif')->nullable();
            $table->string('destino')->nullable();
            $table->string('fac_client')->nullable();
            $table->string('agentes_aduana')->nullable();
            $table->string('almacen')->nullable();
            $table->text('contenedores')->nullable();
            $table->string('canal')->nullable();
            $table->string('dua_f_reg')->nullable();
            $table->string('dua_agent_aduana')->nullable();
            $table->date('fec_entrega_client')->nullable();
            $table->text('incidencias')->nullable();
            $table->enum('status', ['ACTIVO', 'INACTIVO'])->default('ACTIVO')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logistics');
    }
}
