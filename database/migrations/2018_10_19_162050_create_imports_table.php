<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imp');
            $table->date('eta_callao')->nullable();
            $table->date('descarga')->nullable();
            $table->string('consignee')->nullable();
            $table->string('shipper')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('fac_client')->nullable();//factura del cliente
            $table->string('nro_manif')->nullable();
            $table->string('booking')->nullable();
            $table->string('nmbl')->nullable();
            $table->string('nhbl')->nullable();
            $table->integer('cantidad')->nullable();
            $table->string('tamanho', 20)->nullable();
            $table->enum('logistico', ['SI', 'NO'])->default('NO')->nullable();
            $table->string('linea')->nullable();
            $table->string('tipo')->nullable();
            $table->string('tipo_import')->nullable();
            $table->string('mn')->nullable();
            $table->string('pol')->nullable();//antes origen
            $table->string('pais')->nullable();
            $table->string('pod')->nullable();//puerto de descarga
            $table->string('almacen')->nullable();
            $table->text('contenedores')->nullable();
            $table->string('agentes_carga')->nullable();
            $table->string('agentes_aduana')->nullable();
            $table->string('dua')->nullable();
            $table->string('canal')->nullable();
            $table->text('incidencias')->nullable();
            $table->enum('status', ['ACTIVO', 'INACTIVO'])->default('ACTIVO')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imports');
    }
}
