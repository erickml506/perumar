echo "Setting dev config..."
mv .env .env.heroku
mv .env.erick.home .env

echo "Starting virtual machine"
cd ~/Homestead/
vagrant up
vagrant ssh